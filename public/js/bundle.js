/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);

__webpack_require__(2);

__webpack_require__(3);

__webpack_require__(4);

__webpack_require__(5);

__webpack_require__(6);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    var elemCarousel = document.querySelector('#js-carousel');
    var elemCarouselSwitch = document.querySelector('#js-carousel-switch');
    if (elemCarousel === null || elemCarouselSwitch === null) {
        return;
    }
    var arElemsCarouselImg = elemCarousel.querySelectorAll('.js-carousel-img');
    var arElemsCarouselSwitchBtn = elemCarouselSwitch.querySelectorAll('.js-carousel-switch-btn');

    elemCarousel.onclick = function (e) {
        var target = e.target;
        if (target.tagName === 'IMG') {
            for (var i = 0; i < arElemsCarouselImg.length; i++) {
                if (target === arElemsCarouselImg[i]) {
                    hideImg(i);
                    var nextIndex = i === arElemsCarouselImg.length - 1 ? 0 : i + 1;
                    showImg(nextIndex);
                }
            }
        } else {
            if (target.parentElement === elemCarouselSwitch) {
                for (var _i = 0; _i < arElemsCarouselSwitchBtn.length; _i++) {
                    if (target === arElemsCarouselSwitchBtn[_i]) {
                        showImg(_i);
                    } else {
                        hideImg(_i);
                    }
                }
            }
        }
    };

    function showImg(i) {
        arElemsCarouselImg[i].classList.remove('carousel__img_hidden');
        arElemsCarouselImg[i].classList.add('carousel__img_visible');
        arElemsCarouselSwitchBtn[i].classList.add('carousel-switch__itm_active');
    }

    function hideImg(i) {
        arElemsCarouselImg[i].classList.remove('carousel__img_visible');
        arElemsCarouselImg[i].classList.add('carousel__img_hidden');
        arElemsCarouselSwitchBtn[i].classList.remove('carousel-switch__itm_active');
    }
})();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    var button = document.querySelector('#js-devices-list-button');
    if (button === null) {
        return;
    }

    var wrapper = document.querySelector('#js-devices-list-wrapper');
    button.onclick = function () {
        button.disabled = true;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', button.dataset.href);
        xhr.onload = function () {
            var response = JSON.parse(xhr.responseText);
            wrapper.insertAdjacentHTML('beforeend', response.html);
            if (!response.button.toDisplay) {
                button.classList.add('hdn');
            }
            button.innerHTML = response.button.text;
            button.dataset.href = response.button.href;
            button.disabled = false;
        };
        xhr.send();
    };
})();

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    var sortWrapper = document.querySelector('#js-devices-sort-wrapper');
    if (sortWrapper === null) {
        return;
    }

    var button = document.querySelector('#js-devices-list-button');
    var listWrapper = document.querySelector('#js-devices-list-wrapper');
    sortWrapper.onclick = function (e) {
        var target = e.target;
        if ('LI' !== target.tagName || target.classList.contains('sort_itm_active') || sortWrapper.dataset.disabled) {
            return;
        }

        sortWrapper.dataset.disabled = '1';
        for (var i = 0; i < sortWrapper.children.length; i++) {
            sortWrapper.children[i].classList.remove('sort_itm_active');
        }
        target.classList.add('sort_itm_active');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', target.dataset.href);
        xhr.onload = function () {
            var response = JSON.parse(xhr.responseText);
            listWrapper.innerHTML = '';
            listWrapper.insertAdjacentHTML('afterbegin', response.html);
            if (button !== null) {
                button.classList.remove('hdn');
                button.innerHTML = response.button.text;
                button.dataset.href = response.button.href;
            }
            sortWrapper.dataset.disabled = '';
        };
        xhr.send();
    };
})();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    var border = 1;
    var leftMenuElem = document.querySelector('#js-l-menu');
    var leftMenuWrElem = document.querySelector('#js-l-menu-wr');
    var leftMenuElemHeight = leftMenuWrElem.offsetHeight;
    var aOriginal = void 0,
        aCurrent = void 0,
        bOriginal = void 0,
        bCurrent = void 0;
    aOriginal = aCurrent = leftMenuWrElem.getBoundingClientRect().top + window.pageYOffset;
    bOriginal = bCurrent = leftMenuWrElem.getBoundingClientRect().bottom + window.pageYOffset;
    var previousScroll = 0;

    function reset() {
        leftMenuElem.style.position = '';
        leftMenuElem.style.top = '';
        aCurrent = aOriginal;
        bCurrent = bOriginal;
    }

    window.onscroll = function () {
        var scrollDifference = window.pageYOffset - previousScroll;
        previousScroll = window.pageYOffset;
        if (window.pageYOffset < aOriginal + border) {
            reset();

            return;
        }
        if (leftMenuElemHeight <= document.documentElement.clientHeight + border * 2) {
            if (scrollDifference > 0) {
                // console.log('вниз');
                if (window.pageYOffset > aOriginal + border) {
                    leftMenuElem.style.position = 'fixed';
                    leftMenuElem.style.top = -border + 'px';
                }
            }

            return;
        }
        if (scrollDifference > 0) {
            // console.log('вниз');
            if (window.pageYOffset + document.documentElement.clientHeight > bCurrent - border) {
                leftMenuElem.style.position = 'fixed';
                leftMenuElem.style.top = document.documentElement.clientHeight - leftMenuElemHeight + border + 'px';
                bCurrent = window.pageYOffset + document.documentElement.clientHeight + border;
                aCurrent = bCurrent - leftMenuElemHeight;

                return;
            }
            if (leftMenuElem.style.position === 'fixed') {
                leftMenuElem.style.top = aCurrent - window.pageYOffset + 'px';
            }

            return;
        }

        // console.log('вверх');
        if (window.pageYOffset < aCurrent + border) {
            leftMenuElem.style.top = -border + 'px';
            aCurrent = window.pageYOffset - border;
            bCurrent = aCurrent + leftMenuElemHeight;

            return;
        }
        leftMenuElem.style.top = aCurrent - window.pageYOffset + 'px';
    };
})();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    var elemSlider = document.querySelector('.js-slider');
    if (elemSlider === null) {
        return;
    }
    var elemFill = elemSlider.querySelector('.js-slider-bar-fill');
    var elemFlask = elemSlider.querySelector('.js-slider-bar-flask');
    var elemMin = elemSlider.querySelector('.js-slider-min');
    var elemThumb = elemSlider.querySelector('.js-slider-thumb');

    var countSteps = (elemSlider.dataset.max - elemSlider.dataset.min) / elemSlider.dataset.stepLength + 1;
    var offsetStep = (elemFlask.offsetWidth - elemThumb.offsetWidth) / (countSteps - 1);
    var activeSliderWidth = elemFlask.offsetWidth - elemThumb.offsetWidth + 1;

    var arAssocOffsetAndStep = [];
    for (var i = 0; i < activeSliderWidth; i++) {
        arAssocOffsetAndStep[i] = Math.ceil((i - offsetStep / 2) / offsetStep);
    }

    var arOffsetSteps = [];
    for (var _i = 0; _i < countSteps; _i++) {
        arOffsetSteps[_i] = offsetStep * _i;
    }

    elemThumb.onmousedown = function (e) {
        var coordsSliderThumb = getCoords(elemThumb);
        var shiftX = e.pageX - coordsSliderThumb.left;
        var coordsSliderBar = getCoords(elemFlask);

        document.onmousemove = function (e) {
            var newLeft = e.pageX - shiftX - coordsSliderBar.left;
            if (newLeft < 0) {
                newLeft = 0;
            }
            var rightEdgeSliderBar = elemFlask.offsetWidth - elemThumb.offsetWidth;
            if (newLeft > rightEdgeSliderBar) {
                newLeft = rightEdgeSliderBar;
            }
            newLeft = Math.round(newLeft);

            var pos = arAssocOffsetAndStep[newLeft];
            elemThumb.style.left = arOffsetSteps[pos] + 'px';
            elemMin.innerHTML = pos * elemSlider.dataset.stepLength + +elemSlider.dataset.min;
            elemFill.style.width = elemFlask.offsetWidth - pos * offsetStep + 'px';
            elemFill.style.marginLeft = pos * offsetStep + 'px';
        };

        document.onmouseup = function () {
            document.onmousemove = document.onmouseup = null;
        };

        return false;
    };

    elemThumb.ondragstart = function () {
        return false;
    };

    function getCoords(elem) {
        var box = elem.getBoundingClientRect();
        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        };
    }
})();

/***/ })
/******/ ]);