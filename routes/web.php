<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix(getenv('APP_ADMIN_URL'))
    ->namespace('Auth')
    ->group(static function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });

Route::prefix(getenv('APP_ADMIN_URL'))
    ->namespace('Admin')
    ->as('admin.')
    ->middleware('auth')
    ->group(static function () {
        Route::get('/', 'AdminController')->name('index');
        Route::resource('device', 'AdminDeviceController')->except(['show']);
        Route::resource('device/{deviceId}/video', 'AdminVideoController')->except(['show']);
        Route::resource('device/{device}/photo', 'AdminPhotoController')->except(['edit', 'show', 'update']);
        Route::resource('review', 'AdminReviewController')->except(['create', 'show', 'store']);
    });

Route::get('/', 'IndexController@index')->name('index');
Route::get('sitemap', 'SitemapController');
Route::post('review', 'ReviewController@store')->name('review.store');

Route::prefix('{deviceType}')
    ->group(static function () {
        Route::get('', 'TypeController');
        Route::get('search', 'SearchController@filters')->name('device.search');
        Route::prefix('{brandSlug}')
            ->group(static function () {
                Route::get('', 'BrandController');
                Route::get('{deviceSlug}', 'DeviceController');
            });
    });
