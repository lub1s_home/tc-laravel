<div id="photo"></div>
<section class="blk__sect">
    <header>
        <h2 class="title">Фото</h2>
    </header>
    <p class="desc blk__itm">Клик по фото - смотреть следующее фото</p>
    <div class="carousel blk__itm" id="js-carousel">
        @foreach ($photos as $photo)
            <img
                    class="carousel__img @if ($loop->first) carousel__img_first carousel__img_visible @else carousel__img_hidden @endif js-carousel-img"
                    src="{{ $photo }}"
                    alt="{{ $view['device']->typeNameSingular }} {{ $view['device']->brandName }} {{ $view['device']->name }}"
                    itemprop="image"
            >
        @endforeach
        <div class="carousel__switch carousel-switch" id="js-carousel-switch">
            @foreach ($photos as $photo)
                <div class="carousel-switch__itm @if ($loop->first) carousel-switch__itm_active @endif js-carousel-switch-btn"></div>
            @endforeach
        </div>
    </div>
</section>