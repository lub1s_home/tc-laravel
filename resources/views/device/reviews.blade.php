<div id="reviews"></div>
<section class="blk__sect">
    <header>
        <h2 class="title">Отзывы</h2>
    </header>
    <p class="desc blk__itm">Мнения реальных владельцев {{ $view['brand']->name }} {{ $view['device']->name }}</p>
    @foreach ($view['reviews'] as $review)
        <article class="review blk__itm" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <footer>
                <strong itemprop="author">{{ $review->name }}</strong><br>
                <time class="desc" itemprop="datePublished">{{ $review->formatted_created_at }}</time>
            </footer>
            <p class="review__txt" itemprop="reviewBody">{{ $review->review }}</p>
        </article>
    @endforeach
</section>