<div id="video"></div>
<section class="blk__itm blk__sect">
    <h2 class="title">Видео-обзоры</h2>
    @foreach ($view['videos'] as $video)
        <div class="blk__itm">
            <iframe
                    allowfullscreen=""
                    class="video"
                    frameborder="0"
                    mozallowfullscreen=""
                    src="https://www.youtube.com/embed/{{ $video->source }}"
                    webkitallowfullscreen=""
            ></iframe>
        </div>
    @endforeach
</section>