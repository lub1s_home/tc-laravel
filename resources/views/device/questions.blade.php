<div id="assessment"></div>
<section class="blk__itm blk__sect" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
    <h2 class="title">Оценка</h2>
    <p class="desc blk__itm">
        Средняя оценка устройства пользователями <span itemprop="ratingValue">{{ $view['commonRating'] }}</span>
        на основе <span itemprop="reviewCount">{{ count($view['reviews']) }}</span> {{ $view['reviewsCountWordForm'] }}
    </p>
    <div class="blk__itm">
        @foreach ($view['questions'] as $question)
            <figure class="assessment blk__itm blk__itm_offset_s">
                <figcaption class="assessment__capt">
                    {{ $question->name }}: {{ $question->averageRating }}
                </figcaption>
                <div class="assessment__scale-external">
                    <div class="assessment__scale-inner" style="width: {{ $question->cssWidth }}px"></div>
                </div>
            </figure>
        @endforeach
    </div>
</section>