@extends('layouts/default')


@section('content')

    <div itemscope itemtype="https://schema.org/Product">

        @include('include/breadCrumb')

        <header class="blk__sect">
            <h1 itemprop="name" class="title title_size_l">
                {{ $view['device']->typeNameSingular }} {{ $view['device']->brandName }} {{ $view['device']->name }}
            </h1>
        </header>

        {{--@include('device/buy')--}}

        @if($photos)
            @include('device/photo')
        @endif

        @include('device.chars.index')

        @if(count($view['videos']))
            @include('device/video')
        @endif

        @if(count($view['reviews']))
            @include('device/questions')

            @include('device/reviews')
        @endif

        @include('device/addReview')

    </div>

@stop