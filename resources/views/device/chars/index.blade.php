<div id="character"></div>
<section class="blk__sect">
    <h2 class="title">Технические характеристики</h2>
    <div class="blk__itm">
        <table class="character" itemprop="description">
            @foreach($view['device']->type->charGroups as $charGroup)
                <?php /** @var App\Models\Chars\CharGroup $charGroup */ ?>
                <tr>
                    <td class="character__td character__group" colspan="2"><strong>{{ $charGroup->name }}:</strong></td>
                </tr>
                @foreach($charGroup->chars as $char)
                    @if($char->deviceChars->count())
                        <tr>
                            <td class="character__td">{{ $char->name }}</td>
                            <td class="character__td">
                                @include('device.chars.' . $char->entity)
                            </td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
        </table>
    </div>
</section>
