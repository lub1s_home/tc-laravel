<?php /** @var App\Models\Chars\Char $char */ ?>

@foreach($char->options as $option)
    @if($option->id == $char->deviceChars[0]->value)
            {{ $option->name }}
        @break
    @endif
@endforeach
