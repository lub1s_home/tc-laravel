<div id="review-add"></div>
<section class="blk__sect">
    <header>
        <h2 class="title">Оставить отзыв</h2>
    </header>
    @if (Session::has('review_sent'))
        <div class="blk__itm success">{{ Session::get('review_sent') }}</div>
    @else
        <div class="review-add blk__itm">
            @if ($errors->any())
                <p>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </p>
            @endif

            {!! Form::open(['route' => 'review.store']) !!}

                {!! Form::text('name', null, [
                    'class' => 'review-add__itm review-add__name',
                    'placeholder' => 'Имя',
                    'required' => '',
                ]) !!}

                {!! Form::textarea('review', null, [
                    'class' => 'review-add__itm review-add__comment',
                    'maxlength' => '3000',
                    'placeholder' => 'Несколько слов о девайсе...',
                    'required' => '',
                    'rows' => 7,
                ]) !!}

                {!! Form::hidden('device_id', $view['device']['id']) !!}

                <div class="rating">
                    @foreach ($view['questions'] as $question)
                        <div>{{ $question->name }}</div>
                        <div class="rating__bar rating-bar">
                            @for ($j = 5; $j > 0; $j--)
                                <input
                                        class="rating-bar__input rating-bar__input_pos_{{ $j }}"
                                        id="star_{{ $j }}_{{ $loop->index }}"
                                        name="rating[{{ $question->id }}]"
                                        required=""
                                        type="radio"
                                        value="{{ $j }}"
                                >
                                <label
                                        class="rating-bar__label"
                                        for="star_{{ $j }}_{{ $loop->index }}"
                                        title="title"
                                ></label>
                            @endfor
                        </div>
                        <div class="clr"></div>
                    @endforeach
                </div>

                {!! Form::submit('Отправить', [
                    'class' => 'review-add__itm review-add__submit',
                    'name' => 'addReview',
                    'onclick' => 'yaCounter36798010.reachGoal(\'addReviewCurrent\'); return true;',
                ]) !!}

            {!! Form::close() !!}
        </div>
    @endif
</section>