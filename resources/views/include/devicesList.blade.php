<?php /** @var App\Models\Device $device */ ?>

@foreach ($view['devicesList']['items'] as $device)
    <article class="device-itm blk__itm">
        <header class="device-itm__hdr">
            <h3 class="device-itm__name">
                <a class="device-itm__lnk" href="{{ $device->href }}">{{ $device->brand->name }} {{ $device->name }}</a>
            </h3>
        </header>
        <figure class="device-itm__fig">
            <a href="{{ $device->href }}#photo">
                <img
                    class="device-itm__img"
                    src="{{ $device->photoSrc }}"
                    alt="{{ $device->brand->name }} {{ $device->name }}"
                >
            </a>
            <figcaption class="device-itm__capt">
                @if ($device->averageRating)
                    <figure
                            class="device-itm__elem assessment"
                            title="Общая оценка: {{ $device->averageRating }} из 5, на основе {{ $device->reviewsCount }} {{ $device->fReviewsCount }}"
                    >
                        <figcaption>
                            <a
                                class="device-itm__lnk"
                                href="{{ $device->href }}#assessment"
                            >Общая оценка: {{ $device->averageRating }}</a>
                        </figcaption>
                        <div class="assessment__scale-external">
                            <div class="assessment__scale-inner" style="width: {{ $device->cssWidth }}px"></div>
                        </div>
                    </figure>
                @endif
                <div class="device-itm__elem">
                    Дата выхода: {{ $device->formatted_release_date }}<br>
                    @if ($device->reviewsCount)
                        <a
                                class="device-itm__lnk"
                                href="{{ $device->href }}#reviews"
                                {{--TODO заменить тайтл--}}
                                title='2 пользователя [падеж] оценили устройство [тип][падеж] на 4 по всем критериям на основе соотношения "цена качество"'
                        >Отзывов: {{ $device->reviewsCount }}</a><br>
                    @endif
                    @if (isset($device->lastReview))
                        <a class="device-itm__lnk" href="{{ $device->href }}#reviews">Последний отзыв:</a>
                    @endif
                </div>

                @if (isset($device->lastReview))
                    <div class="review">
                        <div>
                            <strong>{{ $device->lastReview->name }}</strong> -
                            <time class="desc">{{ $device->lastReview->formattedCreatedAt }}</time>
                        </div>
                        <p class="review__txt review__txt_theme_device-list">
                            {{ $device->lastReview->formattedContent }}
                        </p>
                    </div>
                @endif
            </figcaption>
        </figure>
    </article>
@endforeach