<nav class="bread-crumb desc blk__sect">
    @foreach ($view['breadCrumbs'] as $breadCrumb)
        @if ($loop->last)
            {{ $breadCrumb['name'] }}
        @else
            <a class="bread-crumb__lnk" href="{{ $breadCrumb['href'] }}">{{ $breadCrumb['name'] }}</a> →
        @endif
    @endforeach
</nav>