<section class="blk__sect">
    <header>
        <h1 class="title title_size_l">{{ $view['devicesList']['h1'] }}</h1>
    </header>
    {{--<p class="blk__itm">Сортировать по</p>--}}
    <ul id="js-devices-sort-wrapper" class="blk__itm sort" data-disabled="">
        <li class="sort_itm sort_itm_active" data-href="{{ $view['devicesList']['sortHref'] }}&sort=1">По алфавиту &darr;</li>
        <li class="sort_itm" data-href="{{ $view['devicesList']['sortHref'] }}&sort=2">По дате выхода &uarr;</li>
        <li class="sort_itm" data-href="{{ $view['devicesList']['sortHref'] }}&sort=3">По популярности &uarr;</li>
    </ul>
    <div id="js-devices-list-wrapper">

        @include('include.devicesList')

    </div>
    @if ($view['devicesList']['button']['toDisplay'])
        <button
                id="js-devices-list-button"
                class="cta-btn blk__itm"
                data-href="{{ $view['devicesList']['button']['href'] }}"
        >{{ $view['devicesList']['button']['text'] }}</button>
    @endif
</section>