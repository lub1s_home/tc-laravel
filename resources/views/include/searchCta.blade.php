<section class="blk__sect">
    <header>
        <h1 class="title title_size_l">Найти {{ $view['type']->name_accusative }} по параметрам</h1>
    </header>
    <a class="cta-btn blk__itm" href="/device_types/search/{{ $view['searchCta']['href'] }}">Найти</a>
</section>