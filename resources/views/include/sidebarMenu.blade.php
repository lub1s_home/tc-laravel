<nav class="blk__sect">
    <h2 class="blk__itm blk__itm_offset_l blk__hdr_size_md">Меню</h2>
    <ul class="blk__itm">
        <li class="l-menu__itm">
            <a href="/" class="l-menu__lnk l-menu__lnk_selected">Что тут интересного?</a>
            <ul class="l-menu__inner">
                @foreach ($view['sidebarMenu'] as $type)
                    <li class="l-menu__itm">
                        @if (empty($type->brands))
                            <a class="l-menu__lnk" href="/{{ $type->slug }}">{{ $type->name_plural }}</a>
                        @else
                            <a class="l-menu__lnk l-menu__lnk_selected" href="/{{ $type->slug }}">{{ $type->name_plural }}</a>
                            <ul class="l-menu__inner">
                                @foreach ($type->brands as $brand)
                                    <li class="l-menu__itm">
                                        @if (empty($brand->devicesList))
                                            <a class="l-menu__lnk" href="/{{ $type->slug }}/{{ $brand->slug }}">{{ $brand->name }}</a>
                                        @else
                                            <a class="l-menu__lnk l-menu__lnk_selected" href="/{{ $type->slug }}/{{ $brand->slug }}">{{ $brand->name }}</a>
                                            <ul class="l-menu__inner">
                                                @foreach ($brand->devicesList as $device)
                                                    <li class="l-menu__itm">
                                                        @if (!$device->isCurrent)
                                                            <a class="l-menu__lnk" href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}">{{ $device->name }}</a>
                                                        @else
                                                            <a class="l-menu__lnk l-menu__lnk_selected" href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}">{{ $device->name }}</a>
                                                            <ul class="l-menu__inner">
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#buy" class="l-menu__lnk">Где купить</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#photo" class="l-menu__lnk">Фото</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#character" class="l-menu__lnk">Характеристики</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#video" class="l-menu__lnk">Видео-обзоры</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#assessment" class="l-menu__lnk">Оценка</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#reviews" class="l-menu__lnk">Отзывы</a>
                                                                </li>
                                                                <li class="l-menu__itm">
                                                                    <a href="/{{ $type->slug }}/{{ $brand->slug }}/{{ $device->slug }}#review-add" class="l-menu__lnk">Оставить отзыв</a>
                                                                </li>
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </li>
    </ul>
</nav>
