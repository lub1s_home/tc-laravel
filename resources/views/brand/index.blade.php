@extends('layouts/default')

@section('content')

    @include('include/breadCrumb')

    @include('include/searchCta')

    @include('include/devicesListWrapper')

@stop
