@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::model($view['video'], [
        'action' => ['Admin\AdminVideoController@update', $view['video']->device_id, $view['video']->id],
        'method' => 'PATCH',
    ]) !!}

        @include('admin.video.formFields', ['submitButtonText' => 'Сохранить'])

    {!! Form::close() !!}

@stop