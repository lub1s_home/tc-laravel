<div class="form-group">
    {!! Form::label('device_id', 'Device id:') !!}
    {!! Form::text('device_id', $view['deviceId'], ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group">
    {!! Form::label('source', 'Source:') !!}
    {!! Form::text('source', null, ['autocomplete' => 'off', 'class' => 'form-control']) !!}
</div>

{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}