@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::open(['action' => ['Admin\AdminVideoController@store', $view['deviceId']]]) !!}

        @include('admin.video.formFields', ['submitButtonText' => 'Создать'])

    {!! Form::close() !!}

@stop