@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @if (count($videos))
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">device id</th>
                    <th scope="col">source</th>
                    <td scope="col" class="text-right">
                        <span class="text-secondary">Удалить</span>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach ($videos as $video)
                    <tr>
                        <th scope="row">
                            <a
                                    href="{{ route('admin.video.edit', [$video->device_id, $video->id]) }}"
                            >{{ $video->id }}</a>
                        </th>
                        <td>{{ $video->device_id }}</td>
                        <td>{{ $video->source }}</td>
                        <td>

                            @include('admin.include.deleteXButton', ['dataId' => $video->id])

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @include('admin.include.confirmDelete', ['formAction' => route('admin.video.index', $view['deviceId'])])

    @endif

    <a
            class="btn btn-primary"
            href="{{ route('admin.video.create', $view['deviceId']) }}"
            role="button"
    >Создать</a>

@stop