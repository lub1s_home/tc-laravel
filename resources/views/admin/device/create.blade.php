@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::open(['route' => 'admin.device.store']) !!}

        @include('admin.device.form-fields', ['submitButtonText' => 'Создать'])

    {!! Form::close() !!}

@stop
