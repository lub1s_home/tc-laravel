<?php /** @var App\Models\Device $device */?>

<div class="form-group">
    {!! Form::label('type_id', 'Type:') !!}
    {!! Form::select('type_id', $view['typesAssoc'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('brand_id', 'Brand:') !!}
    {!! Form::select('brand_id', $view['brandsAssoc'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('release_date', 'Release date:') !!}
    {!! Form::date(
        'release_date',
        isset($device->release_date) ? $device->release_date->formatLocalized('%Y-%m-%d') : null,
        ['class' => 'form-control']
    ) !!}
</div>

<div class="form-check">
    {!! Form::checkbox('is_published', null, null, ['class' => 'form-check-input', 'id' => 'is_published']) !!}
    {!! Form::label('is_published', 'Is published:') !!}
</div>

{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
