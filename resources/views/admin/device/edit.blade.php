<?php /** @var App\Models\Device $device */ ?>

@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::model($device, [
        'action' => ['Admin\AdminDeviceController@update', $device->id],
        'method' => 'PATCH',
    ]) !!}

        @include('admin.device.form-fields', ['submitButtonText' => 'Сохранить'])

    {!! Form::close() !!}

    <hr>

    <p>
        <a href="{{ route('admin.photo.index', $device->id) }}">Фото</a>
        <span
                class="badge badge-{{ $view['countPhoto'] ? 'info' : 'warning' }} badge-pill"
        >{{ $view['countPhoto'] }}</span>
    </p>

    <p>
        <a href="{{ route('admin.video.index', $device->id) }}">Видео</a>
        <span
                class="badge badge-{{ $view['countVideo'] ? 'info' : 'warning' }} badge-pill"
        >{{ $view['countVideo'] }}</span>
    </p>
@stop
