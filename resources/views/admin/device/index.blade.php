<?php /** @var App\Models\Device[] $devices */ ?>

@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>
    <a class="btn btn-primary" href="{{ route('admin.device.create') }}" role="button">Создать</a><br><br>
    @if (count($devices))
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">type</th>
                    <th scope="col">brand</th>
                    <th scope="col">name</th>
                    <th scope="col">release date</th>
                    <th scope="col">last modified</th>
                    <th scope="col"  class="text-secondary">На сайте</th>
                    <th scope="col" class="text-right text-secondary">Удалить</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($devices as $device)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('admin.device.edit', $device->id) }}">{{ $device->id }}</a>
                        </th>
                        <td>{{ $device->type->name_singular }}</td>
                        <td>{{ $device->brand->name }}</td>
                        <td>{{ $device->name }}</td>
                        <td>{{ $device->release_date }}</td>
                        <td>{{ $device->last_modified }}</td>
                        <td>
                            <a
                                href="/{{ $device->type->slug }}/{{ $device->brand->slug }}/{{ $device->slug }}"
                            >Ссылка</a>
                        </td>
                        <td>

                            @include('admin.include.deleteXButton', ['dataId' => $device['id']])

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @include('admin.include.confirmDelete', ['formAction' => route('admin.device.index')])

    @endif

@stop