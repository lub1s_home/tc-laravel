<br>
<ul class="list-group">
    <li class="list-group-item">
        <a href="{{ route('admin.device.index') }}">Девайсы</a>
        <span class="badge badge-info badge-pill">{{ $view['devicesCount'] }}</span>
    </li>
    <li class="list-group-item">
        <a href="{{ route('admin.review.index') }}">Отзывы</a>
        <span class="badge badge-info badge-pill">{{ $view['reviewsCount']['all'] }}</span>
        @if($view['reviewsCount']['isNotPublished'])
            <span class="badge badge-warning badge-pill">{{ $view['reviewsCount']['isNotPublished'] }}</span>
        @endif
    </li>
    <li class="list-group-item">
        <a
                class="text-warning"
                href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
        >Выйти</a>
        {!! Form::open(['route' => 'logout', 'id' => 'logout-form']) !!}
        {!! Form::close() !!}
    </li>
</ul>