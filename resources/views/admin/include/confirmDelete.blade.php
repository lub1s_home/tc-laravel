<div
        class="modal fade"
        id="defaultModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="defaultModalLabel"
        aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="defaultModalLabel">Внимание!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            {!! Form::open([
                'url' => $formAction,
                'id' => 'defaultModalForm',
                'method' => 'delete',
            ]) !!}

                <div class="modal-body" id="idModalBody"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>

                    {!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
                </div>

            {!! Form::close() !!}

        </div>
    </div>
</div>
<script>
    $('#defaultModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        let recipient = button.data('id');
        let modal = $(this);
        modal.find('#defaultModalForm').attr(
            'action',
            '{{ $formAction }}/' + recipient
        );
        modal.find('#idModalBody').text('Удалить элемент # ' + recipient + '?');
        modal.find('#id').val(recipient);
    })
</script>