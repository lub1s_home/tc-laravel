@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @if (count($reviewsList))
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">is published</th>
                    <th scope="col">name</th>
                    <th scope="col">review</th>
                    <th scope="col">created at</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($reviewsList as $review)
                <tr>
                    <th scope="row">
                        <a href="{{ route('admin.review.edit', $review->id) }}">{{ $review->id }}</a>
                    </th>
                    <td>
                        <span
                                class="badge badge-{{ $review->is_published ? 'success' : 'warning' }} badge-pill"
                        >{{ $review->is_published }}</span>
                    </td>
                    <td>{{ $review->name }}</td>
                    <td>{{ $review->formattedReview }}</td>
                    <td>{{ $review->formattedCreatedAt }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@stop