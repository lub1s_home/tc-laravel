@extends('admin/layouts/default')

@section('content')
    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::model($view['review'], [
        'action' => ['Admin\AdminReviewController@update', $view['review']->id],
        'method' => 'PATCH',
    ]) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('review', 'Review:') !!}
            {!! Form::textarea('review', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('is_published', 'Is published:') !!}
            {!! Form::select('is_published', [0, 1], null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
            @include('admin.include.deleteButton', ['dataId' => $view['review']->id])
        </div>

    {!! Form::close() !!}

    @include('admin.include.confirmDelete', ['formAction' => route('admin.review.index')])

@stop