<?php /** @var App\Models\Device $device */ ?>

@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @if (count($view['photos']))
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">File name</th>
                    <th scope="col">Photo</th>
                    <td scope="col" class="text-right">
                        <span class="text-secondary">Удалить</span>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach ($view['photos'] as $photo)
                    <tr>
                        <th scope="row">{{ $photo['baseName'] }}</th>
                        <td><img src="{{ asset($photo['src']) }}" alt=""></td>
                        <td>

                            @include('admin.include.deleteXButton', ['dataId' => $photo['fileName']])

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @include('admin.include.confirmDelete', ['formAction' => route('admin.photo.index', $device->id)])

    @endif

    <p>
        <a
                class="btn btn-primary"
                href="{{ route('admin.photo.create', $device->id) }}"
                role="button"
        >Загрузить</a>
    </p>

@stop