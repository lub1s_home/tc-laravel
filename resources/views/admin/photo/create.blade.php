<?php /** @var App\Models\Device $device */ ?>

@extends('admin/layouts/default')

@section('content')

    <h1>{{ $view['title'] }}</h1>

    @include('admin.include.errorsList')

    {!! Form::open([
        'action' => ['Admin\AdminPhotoController@store', $device->id],
        'enctype' => 'multipart/form-data',
    ]) !!}

        <div class="form-group">
            {!! Form::label('photo', 'Фото:') !!}
            {!! Form::file('photo', ['class' => 'form-control-file']) !!}
        </div>

        {!! Form::submit('Загрузить', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@stop