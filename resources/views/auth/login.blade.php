<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="author" content="Алексей Курза">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
            crossorigin="anonymous"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
            rel="stylesheet"
    >
    <script
            crossorigin="anonymous"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    ></script>
    <script
            crossorigin="anonymous"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    ></script>
    <script
            crossorigin="anonymous"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    ></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-3">

        </div>
        <div class="col-6">

            {{ Form::open(['route' => 'login']) }}

            <div class="form-group">
                {!! Form::label('name', 'E-Mail Address:') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('name', 'Password:') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>

            {!! Form::submit('Login', ['class' => 'btn btn-primary']) !!}

            {{ Form::close() }}

        </div>
    </div>
</div>

</body>
</html>
