<section class="blk__sect">
    <header class="blk__itm">
        <h1 class="title title_size_l">Что Вас интересует?</h1>
    </header>
    <nav>
        @foreach ($view['types'] as $type)
            <article class="img-menu-itm blk__itm">
                <header class="img-menu-itm__hdr">
                    <h3 class="img-menu-itm__name">
                        <a
                                class="img-menu-itm__lnk"
                                href="/{{ $type->slug }}"
                        >{{ $type->name_plural }}</a>
                    </h3>
                </header>
                <figure class="img-menu-itm__fig">
                    <a href="/{{ $type->slug }}">
                        <img
                                class="img-menu-itm__img"
                                src="/img/menu/device_types/{{ $type->slug }}.jpg"
                                alt="{{ $type->name_plural }}"
                        >
                    </a>
                    <figcaption class="img-menu-itm__capt">
                        <div class="img-menu-itm__elem">
                            Брендов: {{ $type->brandsCount }}<br>
                            Моделей: {{ $type->devicesCount }}<br>
                            Отзывов: {{ $type->reviewsCount }}
                        </div>
                    </figcaption>
                </figure>
            </article>
        @endforeach
    </nav>
    <div class="clr"></div>
</section>