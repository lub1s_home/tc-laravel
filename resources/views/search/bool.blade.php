<?php /** @var App\Models\Chars\Char $char */ ?>

<div class="search-params__itm">
    <div>{{ $char->name }}</div>

    <input type="radio" id="char_{{ $char->id }}_no_matter" name="char_{{ $char->id }}" value="no_matter" checked />
    <label for="char_{{ $char->id }}_no_matter">не важно</label><br>

    <input type="radio" id="char_{{ $char->id }}_yes" name="char_{{ $char->id }}" value="yes" />
    <label for="char_{{ $char->id }}_yes">да</label><br>

    <input type="radio" id="char_{{ $char->id }}_no" name="char_{{ $char->id }}" value="no" />
    <label for="char_{{ $char->id }}_no">нет</label>
</div>
