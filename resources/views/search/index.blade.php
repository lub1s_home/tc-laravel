<?php /** @var App\Models\Type $deviceType */ ?>

@extends('layouts/default')

@section('content')

    @include('include/breadCrumb')

    <header class="blk__sect">
        <h1 class="title title_size_l">{{ $view['search']['h1'] }}</h1>
    </header>

    <section class="blk__sect">
        @foreach($deviceType->charGroups as $charGroup)
            <h2 class="title">{{ $charGroup->name }}</h2>
            @foreach($charGroup->chars as $char)
                @include('search.' . $char->entity)
            @endforeach
        @endforeach
        <div class="clr"></div>
    </section>

@stop
