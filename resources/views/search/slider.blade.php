<?php /** @var App\Models\Chars\Char $char */ ?>

<div class="search-params__itm">
    <div
            class="slider js-slider"
            data-max="{{ $char->max_value }}"
            data-min="{{ $char->min_value }}"
            data-step-length="5"
    >
        <div class="slider__row">
            <strong>{{ $char->name }}</strong>
        </div>
        <div class="slider__row">
            <div class="slider__value-half">
                <span class="desc">от</span>
                <span class="js-slider-min slider__value">{{ $char->min_value }}</span>
            </div>
            <div class="slider__value-half">
                <span class="desc">до</span>
                <span class="slider__value">{{ $char->max_value }}</span>
                <span class="slider__value-units desc">{{ $char->unit }}</span>
            </div>
        </div>
        <div class="slider__row">
            <div class="slider__bar">
                <div class="js-slider-thumb slider__thumb slider__thumb_side_left"></div>
                <div class="js-slider-bar-flask slider__bar-flask">
                    <div class="js-slider-bar-fill slider__bar-fill"></div>
                </div>
                <div class="slider__thumb slider__thumb_side_right"></div>
            </div>
        </div>
    </div>

    <input type="text" value="{{ $char->min_value }}" name="char_{{ $char->id }}_min"><br>
    <input type="text" value="{{ $char->max_value }}" name="char_{{ $char->id }}_max">
</div>
