<?php /** @var App\Models\Chars\Char $char */ ?>

<div class="search-params__itm">
    <div><strong>{{ $char->name }}</strong></div>

    <input
            type="radio"
            id="char_{{ $char->id }}_no_matter"
            name="char_{{ $char->id }}"
            value="no_matter"
            checked
    />
    <label for="char_{{ $char->id }}_no_matter">не важно</label><br>

    @foreach($char->options as $option)
        <input
            type="radio"
            id="char_{{ $char->id }}_{{ $option->id }}"
            name="char_{{ $char->id }}"
            value="{{ $option->id }}"
        />
        <label for="char_{{ $char->id }}_{{ $option->id }}">{{ $option->name }}</label><br>
    @endforeach
</div>
