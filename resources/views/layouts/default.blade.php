<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>{{ $view['seo']['title'] }}</title>
        <meta charset="UTF-8">
        <meta name="description" content="{{ $view['seo']['description'] }}">
        <meta name="keywords" content="{{ $view['seo']['keywords'] }}">
        <meta name="author" content="Алексей Курза">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="canonical" href="{{ $view['seo']['canonical'] }}">
        <link rel="stylesheet" href="/css/reset.css">
        <link rel="stylesheet" href="/css/bundle.css">
    </head>
    <body class="body">

        <div class="event">Console</div>

        <div class="main-cont">
            <header class="blk hdr body__block">
                <a class="logo blk__sect blk__sect_offset-t_xl" href="/">
                    <span class="logo__first-word"><span class="logo__first-letter">t</span>echno</span><br>
                    <span class="logo__second-word">comments</span>
                </a>
                <div class="desc blk__sect blk__sect_offset-t_m">
                    Отзывы о компьютерной, мобильной технике, комплектующих и аксессуарах<br>
                    Характеристики, фото, видео, обзоры, оценки, отзывы, мнения о моделях от реальных владельцев
                </div>
                <div class="blk__sect"></div>
            </header>

            <div class="l-menu-wr" id="js-l-menu-wr">
                <div class="l-menu blk" id="js-l-menu">

                    @include ('include/sidebarMenu')

                    <aside class="ad blk__sect">
                        реклама
                    </aside>

                    <div class="blk__sect"></div>
                </div>
            </div>

            <main class="blk main-cont__child_right body__block">

                @yield ('content')

                <div class="blk__sect"></div>
            </main>

            <footer class="blk main-cont__child_right ftr">
                <div class="blk__sect">
                    <div class="blk__itm">
                        Сотрудничество: <span class="ftr__email">forwebmasterak@gmail.com</span>
                    </div>
                    <div class="blk__itm">
                        Релиз Technocomments: 14 апреля 2016
                    </div>
                    <div class="blk__itm">
                        ...
                    </div>
                </div>

                <div class="blk__sect"></div>
            </footer>
        </div>

        <script src="/js/bundle.js"></script>
    </body>
</html>