<section class="blk__sect">
    <header class="blk__itm">
        <h1 class="title title_size_l">{{ $view['type']->name_singular }} какого бренда Вас интересует?</h1>
    </header>
    <nav>
        @foreach ($view['brands'] as $brand)
            <article class="img-menu-itm blk__itm">
                <header class="img-menu-itm__hdr">
                    <h3 class="img-menu-itm__name">
                        <a
                                class="img-menu-itm__lnk"
                                href="/{{ $view['type']->slug }}/{{ $brand->slug }}"
                        >{{ $brand->name }}</a>
                    </h3>
                </header>
                <figure class="img-menu-itm__fig">
                    <a href="/{{ $view['type']->slug }}/{{ $brand->slug }}">
                        <img
                                class="img-menu-itm__img"
                                src="/img/menu/brands/{{ $brand->slug }}.jpg"
                                alt="{{ $brand->name }}"
                        >
                    </a>
                    <figcaption class="img-menu-itm__capt">
                        <div class="img-menu-itm__elem">
                            Моделей: {{ $brand->devicesCount }}<br>
                            Отзывов: {{ $brand->reviewsCount }}
                        </div>
                    </figcaption>
                </figure>
            </article>
        @endforeach
    </nav>
    <div class="clr"></div>
</section>