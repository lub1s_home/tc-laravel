@extends('layouts/default')

@section('content')

    @include('include/breadCrumb')

    @include('include/searchCta')

    @include('type/contentMenu')

    @include('include/devicesListWrapper')

@stop
