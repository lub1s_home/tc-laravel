(function () {
    let elemSlider = document.querySelector('.js-slider');
    if (elemSlider === null) {
        return;
    }
    let elemFill  = elemSlider.querySelector('.js-slider-bar-fill');
    let elemFlask = elemSlider.querySelector('.js-slider-bar-flask');
    let elemMin   = elemSlider.querySelector('.js-slider-min');
    let elemThumb = elemSlider.querySelector('.js-slider-thumb');

    let countSteps = (elemSlider.dataset.max - elemSlider.dataset.min) / elemSlider.dataset.stepLength + 1;
    let offsetStep = (elemFlask.offsetWidth - elemThumb.offsetWidth) / (countSteps - 1);
    let activeSliderWidth = elemFlask.offsetWidth - elemThumb.offsetWidth + 1;

    let arAssocOffsetAndStep = [];
    for (let i = 0; i < activeSliderWidth; i++) {
        arAssocOffsetAndStep[i] = Math.ceil((i - offsetStep / 2) / offsetStep);
    }

    let arOffsetSteps = [];
    for (let i = 0; i < countSteps; i++) {
        arOffsetSteps[i] = offsetStep * i;
    }

    elemThumb.onmousedown = (e) => {
        let coordsSliderThumb = getCoords(elemThumb);
        let shiftX = e.pageX - coordsSliderThumb.left;
        let coordsSliderBar = getCoords(elemFlask);

        document.onmousemove = (e) => {
            let newLeft = e.pageX - shiftX - coordsSliderBar.left;
            if (newLeft < 0) {
                newLeft = 0;
            }
            let rightEdgeSliderBar = elemFlask.offsetWidth - elemThumb.offsetWidth;
            if (newLeft > rightEdgeSliderBar) {
                newLeft = rightEdgeSliderBar;
            }
            newLeft = Math.round(newLeft);

            let pos = arAssocOffsetAndStep[newLeft];
            elemThumb.style.left = arOffsetSteps[pos] + 'px';
            elemMin.innerHTML = pos * elemSlider.dataset.stepLength + +elemSlider.dataset.min;
            elemFill.style.width = elemFlask.offsetWidth - pos * offsetStep + 'px';
            elemFill.style.marginLeft = pos * offsetStep + 'px';
        };

        document.onmouseup = () => {
            document.onmousemove = document.onmouseup = null;
        };

        return false;
    };

    elemThumb.ondragstart = () => {
        return false;
    };

    function getCoords (elem) {
        let box = elem.getBoundingClientRect();
        return {
            top:  box.top  + pageYOffset,
            left: box.left + pageXOffset,
        };
    }
})();
