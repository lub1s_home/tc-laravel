'use strict';

import '../less/index.less';

import './carousel';
import './devicesList/loading';
import './devicesList/sorting';
import './leftMenu';
import './slider';
