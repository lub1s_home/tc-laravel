(function () {
    const border = 1;
    let leftMenuElem = document.querySelector('#js-l-menu');
    let leftMenuWrElem = document.querySelector('#js-l-menu-wr');
    let leftMenuElemHeight = leftMenuWrElem.offsetHeight;
    let aOriginal, aCurrent, bOriginal, bCurrent;
    aOriginal = aCurrent = leftMenuWrElem.getBoundingClientRect().top + window.pageYOffset;
    bOriginal = bCurrent = leftMenuWrElem.getBoundingClientRect().bottom + window.pageYOffset;
    let previousScroll = 0;

    function reset() {
        leftMenuElem.style.position = '';
        leftMenuElem.style.top = '';
        aCurrent = aOriginal;
        bCurrent = bOriginal;
    }

    window.onscroll = () => {
        let scrollDifference = window.pageYOffset - previousScroll;
        previousScroll = window.pageYOffset;
        if (window.pageYOffset < aOriginal + border) {
            reset();

            return;
        }
        if (leftMenuElemHeight <= document.documentElement.clientHeight + border * 2) {
            if (scrollDifference > 0) {
                // console.log('вниз');
                if (window.pageYOffset > aOriginal + border) {
                    leftMenuElem.style.position = 'fixed';
                    leftMenuElem.style.top = - border + 'px';
                }
            }

            return;
        }
        if (scrollDifference > 0) {
            // console.log('вниз');
            if (window.pageYOffset + document.documentElement.clientHeight > bCurrent - border) {
                leftMenuElem.style.position = 'fixed';
                leftMenuElem.style.top = document.documentElement.clientHeight - leftMenuElemHeight + border + 'px';
                bCurrent = window.pageYOffset + document.documentElement.clientHeight + border;
                aCurrent = bCurrent - leftMenuElemHeight;

                return;
            }
            if (leftMenuElem.style.position === 'fixed') {
                leftMenuElem.style.top = aCurrent - window.pageYOffset + 'px';
            }

            return;
        }

        // console.log('вверх');
        if (window.pageYOffset < aCurrent + border) {
            leftMenuElem.style.top = - border + 'px';
            aCurrent = window.pageYOffset - border;
            bCurrent = aCurrent + leftMenuElemHeight;

            return;
        }
        leftMenuElem.style.top = aCurrent - window.pageYOffset + 'px';
    };
})();
