(function () {
    let sortWrapper = document.querySelector('#js-devices-sort-wrapper');
    if (sortWrapper === null) {
        return;
    }

    let button = document.querySelector('#js-devices-list-button');
    let listWrapper = document.querySelector('#js-devices-list-wrapper');
    sortWrapper.onclick = (e) => {
        let target = e.target;
        if ('LI' !== target.tagName
            || target.classList.contains('sort_itm_active')
            || sortWrapper.dataset.disabled
        ) {
            return;
        }

        sortWrapper.dataset.disabled = '1';
        for (let i = 0; i < sortWrapper.children.length; i++) {
            sortWrapper.children[i].classList.remove('sort_itm_active');
        }
        target.classList.add('sort_itm_active');
        let xhr = new XMLHttpRequest();
        xhr.open('GET', target.dataset.href);
        xhr.onload = () => {
            let response = JSON.parse(xhr.responseText);
            listWrapper.innerHTML = '';
            listWrapper.insertAdjacentHTML('afterbegin', response.html);
            if (button !== null) {
                button.classList.remove('hdn');
                button.innerHTML = response.button.text;
                button.dataset.href = response.button.href;
            }
            sortWrapper.dataset.disabled = '';
        };
        xhr.send();
    };
})();
