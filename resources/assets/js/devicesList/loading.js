(function () {
    let button = document.querySelector('#js-devices-list-button');
    if (button === null) {
        return;
    }

    let wrapper = document.querySelector('#js-devices-list-wrapper');
    button.onclick = () => {
        button.disabled = true;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', button.dataset.href);
        xhr.onload = () => {
            let response = JSON.parse(xhr.responseText);
            wrapper.insertAdjacentHTML('beforeend', response.html);
            if (!response.button.toDisplay) {
                button.classList.add('hdn');
            }
            button.innerHTML = response.button.text;
            button.dataset.href = response.button.href;
            button.disabled = false;
        };
        xhr.send();
    };
})();
