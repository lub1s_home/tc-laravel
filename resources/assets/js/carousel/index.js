(function () {
    let elemCarousel       = document.querySelector('#js-carousel');
    let elemCarouselSwitch = document.querySelector('#js-carousel-switch');
    if (elemCarousel === null || elemCarouselSwitch === null) {
        return;
    }
    let arElemsCarouselImg       = elemCarousel.querySelectorAll('.js-carousel-img');
    let arElemsCarouselSwitchBtn = elemCarouselSwitch.querySelectorAll('.js-carousel-switch-btn');

    elemCarousel.onclick = (e) => {
        let target = e.target;
        if (target.tagName === 'IMG') {
            for (let i = 0; i < arElemsCarouselImg.length; i++) {
                if (target === arElemsCarouselImg[i]) {
                    hideImg(i);
                    let nextIndex = (i === arElemsCarouselImg.length - 1)
                        ? 0
                        : i + 1;
                    showImg(nextIndex);
                }
            }
        } else {
            if (target.parentElement === elemCarouselSwitch) {
                for (let i = 0; i < arElemsCarouselSwitchBtn.length; i++) {
                    if (target === arElemsCarouselSwitchBtn[i]) {
                        showImg(i);
                    } else {
                        hideImg(i);
                    }
                }
            }
        }
    };

    function showImg(i) {
        arElemsCarouselImg[i].classList.remove('carousel__img_hidden');
        arElemsCarouselImg[i].classList.add('carousel__img_visible');
        arElemsCarouselSwitchBtn[i].classList.add('carousel-switch__itm_active');
    }

    function hideImg(i) {
        arElemsCarouselImg[i].classList.remove('carousel__img_visible');
        arElemsCarouselImg[i].classList.add('carousel__img_hidden');
        arElemsCarouselSwitchBtn[i].classList.remove('carousel-switch__itm_active');
    }
})();
