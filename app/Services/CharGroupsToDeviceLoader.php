<?php

namespace App\Services;

use App\Models\Chars\DeviceChar;
use App\Models\Device;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CharGroupsToDeviceLoader
{
    /**
     * @param Device $device
     */
    public function load(Device $device): void
    {
        $device
            ->type
            ->load(['charGroups' => static function (HasMany $query) use ($device) {
                $query
                    ->orderBy('sort')
                    ->with(['chars' => static function (HasMany $query) use ($device) {
                        $query
                            ->orderBy('sort')
                            ->with(['deviceChars' => static function (HasMany $query) use ($device) {
                                /** @var HasMany|DeviceChar $query */
                                $query->whereDeviceId($device->id);
                            }])
                            ->with('options');
                    }]);
            }]);
    }
}
