<?php

namespace App\Services;

use App\Models\Type;
use App\Repositories\Devices\DeviceCharRepository;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CharFiltersToDeviceTypeLoader
{
    /**
     * @var DeviceCharRepository
     */
    private $deviceCharRepo;

    /**
     * CharFiltersToDeviceTypeLoader constructor.
     *
     * @param DeviceCharRepository $deviceCharRepo
     */
    public function __construct(DeviceCharRepository $deviceCharRepo)
    {
        $this->deviceCharRepo = $deviceCharRepo;
    }

    /**
     * @param Type $deviceType
     */
    public function load(Type $deviceType): void
    {
        $deviceType
            ->load(['charGroups' => static function (HasMany $query) {
                $query
                    ->orderBy('sort')
                    ->with(['chars' => static function (HasMany $query) {
                        $query
                            ->orderBy('sort')
                            ->with(['options' => static function (HasMany $query) {
                                $query->orderBy('name');
                            }]);
                    }]);
            }]);

        $ids = [];
        foreach ($deviceType->charGroups as $charGroup) {
            foreach ($charGroup->chars as $char) {
                if ($char->entity !== 'slider') {
                    continue;
                }
                $ids[] = $char->id;
            }
        }

        $map = $this->deviceCharRepo->getCharIdAndMaxMinValueMap($ids);

        foreach ($deviceType->charGroups as $charGroup) {
            foreach ($charGroup->chars as $char) {
                if (!isset($map[$char->id])) {
                    continue;
                }
                $char->setAttribute('min_value', $map[$char->id]['min_value']);
                $char->setAttribute('max_value', $map[$char->id]['max_value']);
            }
        }
    }
}
