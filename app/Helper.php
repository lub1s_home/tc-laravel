<?php

namespace App;

class Helper
{
    public function getWordForm(int $count, array $forms = ['штука', 'штуки', 'штук']): string
    {
        $formsKey = (($count % 10 == 1 && $count % 100 != 11)
            ? 0
            : ($count % 10 >= 2 && $count % 10 <= 4 && ($count % 100 < 10 || $count % 100 >= 20)
                ? 1
                : 2
            )
        );

        return isset($forms[$formsKey]) ? $forms[$formsKey] : '';
    }
}
