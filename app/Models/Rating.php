<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rating extends Model
{
    public $timestamps = false;

    //Атрибуты, которые можно массово задавать.
    protected $fillable = ['review_id', 'question_id', 'value'];

    public function review(): BelongsTo
    {
        return $this->belongsTo('App\Models\Review');
    }

    public function question(): BelongsTo
    {
        return $this->belongsTo('App\Models\Question');
    }
}
