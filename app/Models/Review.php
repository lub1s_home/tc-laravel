<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Review
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $device_id
 * @property bool $is_published
 * @property string $name
 * @property string $review
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read string $formatted_created_at
 * @property-read Device $device
 * @property-read Collection|Rating[] $ratings
 *
 * @method Builder published()
 */
class Review extends Model
{
    /**
     * @inheritDoc
     */
    protected $fillable = [
        'device_id',
        'name',
        'is_published',
        'review',
    ];

    /**
     * @return string
     */
    public function getFormattedCreatedAtAttribute(): string
    {
        $months = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];
        $month = $months[$this->created_at->month];

        return $this->created_at->formatLocalized("%d $month %Y - %H:%M");
    }

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }

    /**
     * @return HasMany
     */
    public function ratings(): HasMany
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where('is_published', true);
    }
}
