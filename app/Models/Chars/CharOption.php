<?php

namespace App\Models\Chars;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CharOption
 *
 * @package App\Models\Chars
 *
 * @property int $id
 * @property int $char_id
 * @property string $name
 *
 * @property-read Char $char
 */
class CharOption extends Model
{
    /**
     * @return BelongsTo
     */
    public function char(): BelongsTo
    {
        return $this->belongsTo(Char::class);
    }
}
