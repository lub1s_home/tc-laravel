<?php

namespace App\Models\Chars;

use App\Models\Device;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Char
 *
 * @package App\Models\Chars
 *
 * @property int $id
 * @property int $group_id
 * @property int $sort
 * @property string $entity
 * @property string $name
 * @property string|null $data
 *
 * @property-read string $unit
 *
 * @property-read CharGroup $group
 * @property-read Collection|Device[] $devices
 * @property-read Collection|DeviceChar[] $deviceChars
 * @property-read Collection|CharOption[] $options
 */
class Char extends Model
{
    /**
     * @return string
     */
    public function getUnitAttribute(): string
    {
        $data = json_decode($this->attributes['data']); //TODO куда-то сохранять декодированные данные

        return $data->unit ?? '';
    }

    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(CharGroup::class);
    }

    /**
     * @return BelongsToMany
     */
    public function devices(): BelongsToMany
    {
        return $this->belongsToMany(Device::class, DeviceChar::TABLE_NAME);
    }

    /**
     * @return HasMany
     */
    public function deviceChars(): HasMany
    {
        return $this->hasMany(DeviceChar::class);
    }

    /**
     * @return HasMany
     */
    public function options(): HasMany
    {
        return $this->hasMany(CharOption::class);
    }
}
