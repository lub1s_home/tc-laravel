<?php

namespace App\Models\Chars;

use App\Models\Device;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Query\Builder;

/**
 * Class CharDevice
 *
 * @package App\Models\Chars
 *
 * @property int $id
 * @property int $device_id
 * @property int $char_id
 * @property float $value
 *
 * @property-read Char $char
 * @property-read Device $device
 *
 * @method Builder|self whereDeviceId(int $deviceId)
 */
class DeviceChar extends Pivot
{
    /**
     * @var string
     */
    public const TABLE_NAME = 'device_chars';

    /**
     * @inheritDoc
     */
    public $incrementing = true;

    /**
     * @inheritDoc
     */
    protected $table = self::TABLE_NAME;

    /**
     * @return BelongsTo
     */
    public function char(): BelongsTo
    {
        return $this->belongsTo(Char::class);
    }

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
}
