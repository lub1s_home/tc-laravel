<?php

namespace App\Models\Chars;

use App\Models\Type;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class CharGroup
 *
 * @package App\Models\Chars
 *
 * @property int $id
 * @property int $sort
 * @property string $name
 *
 * @property-read Type $deviceType
 * @property-read Collection|Char[] $chars
 */
class CharGroup extends Model
{
    /**
     * @return BelongsTo
     */
    public function deviceType(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * @return HasMany
     */
    public function chars(): HasMany
    {
        return $this->hasMany(Char::class, 'group_id');
    }
}
