<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Video
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $device_id
 * @property string $source
 *
 * @property-read Device $device
 *
 * @method Builder|self whereDeviceId(int $deviceId)
 */
class Video extends Model
{
    /**
     * @inheritDoc
     */
    public $timestamps = false;

    /**
     * @inheritDoc
     */
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
}
