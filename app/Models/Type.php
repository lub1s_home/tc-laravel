<?php

namespace App\Models;

use App\Models\Chars\CharGroup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Type
 *
 * @package App\Models
 *
 * @property int $id
 * @property string $name_accusative
 * @property string $name_plural
 * @property string $name_prepositional
 * @property string $name_singular
 * @property string $slug
 *
 * @property-read Collection|CharGroup[] $charGroups
 * @property-read Collection|Device[] $devices
 * @property-read Collection|Question[] $questions
 *
 * @method Builder|self whereSlug(string $slug)
 */
class Type extends Model
{
    /**
     * @inheritDoc
     */
    public $timestamps = false;

    /**
     * @inheritDoc
     */
    protected $guarded = [];

    /**
     * @inheritDoc
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * @return HasMany
     */
    public function charGroups(): HasMany
    {
        return $this->hasMany(CharGroup::class, 'device_type_id');
    }

    /**
     * @return HasMany
     */
    public function devices(): HasMany
    {
        return $this->hasMany(Device::class);
    }

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class);
    }
}
