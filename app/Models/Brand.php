<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Brand
 *
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $site_url
 *
 * @property-read string $slug
 *
 * @property-read Collection|Device[] $devices
 */
class Brand extends Model
{
    use HasSlug;

    /**
     * @return HasMany
     */
    public function devices(): HasMany
    {
        return $this->hasMany(Device::class);
    }
}
