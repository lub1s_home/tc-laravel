<?php

namespace App\Models;

/**
 * Trait HasSlug
 *
 * @package App\Models
 *
 * @property string $name
 * @property-read string $slug
 */
trait HasSlug
{
    /**
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return str_replace(' ', '_', strtolower($this->name));
    }
}
