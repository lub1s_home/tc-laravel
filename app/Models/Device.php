<?php

namespace App\Models;

use App\Models\Chars\Char;
use App\Models\Chars\DeviceChar;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Device
 *
 * @package App\Models
 *
 * @property int $id
 * @property int $type_id
 * @property int $brand_id
 * @property bool $is_published
 * @property string $name
 * @property Carbon $last_modified
 * @property Carbon $release_date
 *
 * @property-read string $slug
 * @property-read string $formatted_release_date
 * @property-read string $path_to_photos
 *
 * @property-read Brand $brand
 * @property-read Type $type
 * @property-read Collection|Char[] $chars
 * @property-read Collection|DeviceChar[] $deviceChars
 * @property-read Collection|Review[] $reviews
 * @property-read Collection|Video[] $videos
 *
 * @method Builder|self published()
 * @method Builder|self whereBrandId(string $brandId)
 * @method Builder|self whereTypeId(string $typeId)
 */
class Device extends Model
{
    use HasSlug;

    /**
     * @inheritDoc
     */
    public $timestamps = false;

    /**
     * @inheritDoc
     */
    protected $guarded = [];

    /**
     * @inheritDoc
     */
    protected $dates = [
        'last_modified',
        'release_date',
    ];

    /**
     * @return string
     */
    public function getFormattedReleaseDateAttribute(): string
    {
        if (!$this->release_date) {
            return '';
        }

        $months = [
            1 => 'январь',
            2 => 'февраль',
            3 => 'март',
            4 => 'апрель',
            5 => 'май',
            6 => 'июнь',
            7 => 'июль',
            8 => 'август',
            9 => 'сентябрь',
            10 => 'октябрь',
            11 => 'ноябрь',
            12 => 'декабрь',
        ];
        $month = $months[$this->release_date->month];

        return $this->release_date->formatLocalized("$month %Y");
    }

    /**
     * @return string
     */
    public function getPathToPhotosAttribute(): string
    {
        return 'devices/' . $this->type->slug . '/' . $this->brand->slug . '/' . $this->slug;
    }

    /**
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * @return BelongsToMany
     */
    public function chars(): BelongsToMany
    {
        return $this
            ->belongsToMany(Char::class, DeviceChar::TABLE_NAME)
            ->withPivot(['value']);
    }

    /**
     * @return HasMany
     */
    public function deviceChars(): HasMany
    {
        return $this->hasMany(DeviceChar::class);
    }

    /**
     * @return HasMany|Review
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    /**
     * @return HasMany
     */
    public function videos(): HasMany
    {
        return $this->hasMany(Video::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->where('devices.is_published', '=', true);
    }
}
