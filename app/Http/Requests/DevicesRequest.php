<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DevicesRequest
 *
 * @package App\Http\Requests
 *
 * @property-read int $type_id
 * @property-read int|null $brand_id
 * @property-read int $page
 * @property-read int $sort
 */
class DevicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type_id' => 'required|integer',
            'brand_id' => 'nullable|integer',
            'page' => 'required|integer',
            'sort' => 'required|integer',
        ];
    }
}
