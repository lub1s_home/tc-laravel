<?php

namespace App\Http\Controllers;

use App\Repositories\DeviceRepository;
use Illuminate\Http\Response;

class SitemapController extends Controller
{
    /**
     * @param DeviceRepository $deviceRepo
     *
     * @return Response
     */
    public function __invoke(DeviceRepository $deviceRepo): Response
    {
        $devices = $deviceRepo->getWithTypeAndBrand();
        $domain = getenv('APP_URL');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($devices as $device) {
            $xml .= '<url>';
            $xml .= '<loc>';
            $xml .= $domain . '/' . $device->type->slug . '/' . $device->brand->slug . '/' . $device->slug;
            $xml .= '</loc>';
            $xml .= '<lastmod>';
            $xml .= $device->last_modified ? $device->last_modified->format('c') : '';
            $xml .= '</lastmod>';
            $xml .= '</url>';
        }
        $xml .= '</urlset>';

        return response($xml)->withHeaders(['Content-Type' => 'text/xml']);
    }
}
