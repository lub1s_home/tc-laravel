<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Services\CharFiltersToDeviceTypeLoader;
use Illuminate\View\View;

class SearchController extends Controller
{
    /**
     * @var CharFiltersToDeviceTypeLoader
     */
    private $charFiltersToDeviceTypeLoader;

    /**
     * SearchController constructor.
     *
     * @param CharFiltersToDeviceTypeLoader $charFiltersToDeviceTypeLoader
     */
    public function __construct(CharFiltersToDeviceTypeLoader $charFiltersToDeviceTypeLoader)
    {
        $this->charFiltersToDeviceTypeLoader = $charFiltersToDeviceTypeLoader;
    }

    /**
     * @param Type $deviceType
     *
     * @return View
     */
    public function filters(Type $deviceType): View
    {
        $this->charFiltersToDeviceTypeLoader->load($deviceType);

        $view = [
            'breadCrumbs' => [
                [
                    'name' => 'Главная',
                    'href' => '/',
                ],
                [
                    'name' => $deviceType->name_plural,
                    'href' => '/' . $deviceType->slug,
                ],
                [
                    'name' => 'Поиск',
                ],
            ],
            'search' => [
                'h1' => $deviceType->name_plural . ' - поиск по характеристикам',
            ],
            'seo' => [
                'canonical' => '',
                'description' => '',
                'keywords' => '',
                'title' => '',
            ],
            'sidebarMenu' => [],
        ];

        return view('search.index', compact(['view', 'deviceType']));
    }
}
