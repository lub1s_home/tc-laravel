<?php

namespace App\Http\Controllers;

use App\Entities\DevicesList;
use App\Models\Type;
use App\Repositories\BrandRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\TypeRepository;
use Illuminate\View\View;

class TypeController extends Controller
{
    private $typeRepo;

    private $brandRepo;

    private $deviceRepo;

    private $devicesList;

    public function __construct(
        TypeRepository $typeRepo,
        BrandRepository $brandRepo,
        DeviceRepository $deviceRepo,
        DevicesList $devicesList
    ) {
        $this->typeRepo = $typeRepo;
        $this->brandRepo = $brandRepo;
        $this->deviceRepo = $deviceRepo;
        $this->devicesList = $devicesList;
    }

    /**
     * @param Type $deviceType
     *
     * @return View
     */
    public function __invoke(Type $deviceType): View
    {
        //Список брендов со статистикой.
        $brands = $this->brandRepo->getListByTypeId($deviceType->id);
        $devicesCounts = $this->deviceRepo->getBrandIdsAndDevicesCountAssoc($deviceType->id);
        $reviewsCounts = $this->deviceRepo->getBrandIdsAndReviewsCountAssoc($deviceType->id);
        foreach ($brands as $brand) {
            $brand->devicesCount = $devicesCounts[$brand->id];
            $brand->reviewsCount = isset($reviewsCounts[$brand->id]) ? $reviewsCounts[$brand->id] : 0;
        }

        //Сайдбар меню.
        $types = $this->typeRepo->getList();
        foreach ($types as $type) {
            $type->brands = ($type->id === $deviceType->id) ? $brands : null;
        }

        //Список девайсов.
        $devices = $this->deviceRepo->getListByTypeId($deviceType->id);
        $this->devicesList->upgrade($devices);
        $devicesCount = $this->deviceRepo->getCountByTypeId($deviceType->id);
        $button = $this->devicesList->getButton($devicesCount, $deviceType->id);

        $view = [
            'breadCrumbs' => [
                [
                    'name' => 'Главная',
                    'href' => '/',
                ],
                [
                    'name' => $deviceType->name_plural,
                    'href' => '/' . $deviceType->slug,
                ],
            ],
            'type' => $deviceType,
            'brands' => $brands,
            'devicesList' => [
                'h1' => $deviceType->name_plural . ' (' . $devicesCount . ' шт)',
                'sortHref' => '/api/ajax?type_id=' . $deviceType->id . '&page=1',
                'button' => $button,
                'items' => $devices,
            ],
            'searchCta' => [
                'href' => '?device_type=1',
            ],
            'seo' => [
                'canonical' => '/' . $deviceType->slug,
                'description' => $deviceType->name_plural . ': новинки, реальные отзывы, оценки, ' .
                    'технические характеристики, обзоры, мнения владельцев, фото, видео.',
                'keywords' => mb_strtolower($deviceType->name_plural) . ', отзывы, мнения, новинки, фото, видео, ' .
                    'обзоры, технические характеристики',
                'title' => $deviceType->name_plural . ' - отзывы',
            ],
            'sidebarMenu' => $types,
        ];

        return view('type.index', ['view' => $view]);
    }
}
