<?php

namespace App\Http\Controllers;

use App\Repositories\DeviceRepository;
use App\Repositories\TypeRepository;
use Illuminate\View\View;

class IndexController extends Controller
{
    private $typeRepo;

    private $deviceRepo;

    public function __construct(TypeRepository $typeRepo, DeviceRepository $deviceRepo)
    {
        $this->typeRepo = $typeRepo;
        $this->deviceRepo = $deviceRepo;
    }

    //Главная страница.
    public function index(): View
    {
        //Список типов со статистикой.
        $types = $this->typeRepo->getList();
        $brandsCounts = $this->deviceRepo->getTypeIdsAndBrandsCountAssoc();
        $devicesCounts = $this->deviceRepo->getTypeIdsAndDevicesCountAssoc();
        $reviewsCounts = $this->deviceRepo->getTypeIdsAndReviewsCountAssoc();
        foreach ($types as $type) {
            $type->brandsCount = $brandsCounts[$type->id];
            $type->devicesCount = $devicesCounts[$type->id];
            $type->reviewsCount = $reviewsCounts[$type->id];
        }

        $view = [
            'breadCrumbs' => [
                0 => [
                    'name' => 'Главная',
                    'href' => '/',
                ],
            ],
            'types' => $types,
            //TODO добавить блок "лучшие девайсы".
            'seo' => [
                'canonical' => '/',
                'description' => 'Отзывы о компьютерной, мобильной технике, периферии и комплектующих. ' .
                    'Реальные мнения владельцев.',
                'keywords' => 'компьютерная техника, устройства, IT, периферия, комплектующие, отзывы, мнения, ' .
                    'новинки, фото, видео, обзоры, технические характеристики',
                'title' => 'Отзывы о компьютерной технике',
            ],
            'sidebarMenu' => $types,
        ];

        return view('index', ['view' => $view]);
    }
}
