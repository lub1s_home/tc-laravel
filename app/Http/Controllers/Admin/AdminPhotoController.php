<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminPhotoRequest;
use App\Models\Device;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class AdminPhotoController extends Controller
{
    private $deviceRepo;

    private $reviewRepo;

    public function __construct(DeviceRepository $deviceRepo, ReviewRepository $reviewRepo)
    {
        $this->deviceRepo = $deviceRepo;
        $this->reviewRepo = $reviewRepo;
    }

    /**
     * @param Device $device
     *
     * @return View
     */
    public function index(Device $device): View
    {
        $photoNames = Storage::disk('public')->allFiles($device->path_to_photos);
        $photos = [];
        foreach ($photoNames as $photoName) {
            $photos[] = [
                'baseName' => pathinfo($photoName, PATHINFO_BASENAME),
                'fileName' => pathinfo($photoName, PATHINFO_FILENAME),
                'src' => Storage::disk('public')->url($photoName),
            ];
        }

        $view = [
            'devicesCount' => $this->deviceRepo->getCount(),
            'photos' => $photos,
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Фото',
        ];

        return view('admin.photo.index', compact(['view', 'device']));
    }

    /**
     * @param Device $device
     *
     * @return View
     */
    public function create(Device $device): View
    {
        $view = [
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Загрузить фото',
        ];

        return view('admin.photo.create', compact(['view', 'device']));
    }

    /**
     * @param AdminPhotoRequest $request
     * @param Device $device
     *
     * @return RedirectResponse
     */
    public function store(AdminPhotoRequest $request, Device $device): RedirectResponse
    {
        $fileName = time() . '.jpg';
        $request->file('photo')->storeAs($device->path_to_photos, $fileName, ['disk' => 'public']);

        return redirect()->route('admin.photo.index', ['deviceId' => $device->id]);
    }

    /**
     * @param Device $device
     * @param int $photoName
     *
     * @return RedirectResponse
     */
    public function destroy(Device $device, int $photoName): RedirectResponse
    {
        $filePath = $device->path_to_photos . '/' . $photoName . '.jpg';
        Storage::disk('public')->delete($filePath);

        return redirect()->route('admin.photo.index', ['deviceId' => $device->id]);
    }
}
