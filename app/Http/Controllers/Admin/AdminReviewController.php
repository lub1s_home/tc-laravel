<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminReviewController extends Controller
{
    private $deviceRepo;

    private $reviewRepo;

    public function __construct(DeviceRepository $deviceRepo, ReviewRepository $reviewRepo)
    {
        $this->deviceRepo = $deviceRepo;
        $this->reviewRepo = $reviewRepo;
    }

    //Список отзывов.
    public function index(): View
    {
        $view = [
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Отзывы',
        ];
        $reviewsList = $this->reviewRepo->getList();

        return view('admin.review.index', compact(['reviewsList', 'view']));
    }

    //Форма редактирования отзыва.
    public function edit(int $id): View
    {
        $review = $this->reviewRepo->findOrFail($id);
        $view = [
            'devicesCount' => $this->deviceRepo->getCount(),
            'review' => $review,
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Отзыв #' . $review->id,
        ];

        return view('admin.review.edit', compact('view'));
    }

    //Обновление отзыва.
    public function update(int $id, Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'review' => 'required',
        ]);

        $review = $this->reviewRepo->findOrFail($id);
        $review->update($request->all());

        return redirect()->route('admin.review.edit', $id);
    }

    /**
     * @param int $id
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        $this->reviewRepo->find($id)->delete();

        return redirect()->route('admin.review.index');
    }
}
