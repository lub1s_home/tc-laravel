<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use Illuminate\View\View;

class AdminController extends Controller
{
    public function __invoke(DeviceRepository $deviceRepo, ReviewRepository $reviewRepo): View
    {
        $view = [
            'devicesCount' => $deviceRepo->getCount(),
            'reviewsCount' => $reviewRepo->getCounts(),
            'title' => 'Панель администратора',
        ];

        return view('admin.index', compact('view'));
    }
}
