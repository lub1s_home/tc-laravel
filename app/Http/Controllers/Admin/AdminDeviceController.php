<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminDeviceRequest;
use App\Models\Device;
use App\Repositories\BrandRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\TypeRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class AdminDeviceController extends Controller
{
    /**
     * @var TypeRepository
     */
    private $typeRepo;

    /**
     * @var BrandRepository
     */
    private $brandRepo;

    /**
     * @var DeviceRepository
     */
    private $deviceRepo;

    /**
     * @var ReviewRepository
     */
    private $reviewRepo;

    /**
     * AdminDeviceController constructor.
     *
     * @param TypeRepository $typeRepo
     * @param BrandRepository $brandRepo
     * @param DeviceRepository $deviceRepo
     * @param ReviewRepository $reviewRepo
     */
    public function __construct(
        TypeRepository $typeRepo,
        BrandRepository $brandRepo,
        DeviceRepository $deviceRepo,
        ReviewRepository $reviewRepo
    ) {
        $this->typeRepo = $typeRepo;
        $this->deviceRepo = $deviceRepo;
        $this->reviewRepo = $reviewRepo;
        $this->brandRepo = $brandRepo;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $devices = $this->deviceRepo->getWithTypeAndBrand();
        $view = [
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Девайсы',
        ];

        return view('admin.device.index', compact(['devices', 'view']));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $view = [
            'brandsAssoc' => $this->brandRepo->getIdAndNameAssoc(),
            'typesAssoc' => $this->typeRepo->getIdAndNameAssoc(),
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Создать девайс',
        ];

        return view('admin.device.create', compact('view'));
    }

    /**
     * @param AdminDeviceRequest $request
     *
     * @return RedirectResponse
     */
    public function store(AdminDeviceRequest $request): RedirectResponse
    {
        $request['is_published'] = isset($request['is_published']) ? 1 : 0;
        $request['last_modified'] = Carbon::now();
        $device = new Device($request->all());
        $device->save();

        return redirect()->route('admin.device.edit', [$device->id]);
    }

    /**
     * @param Device $device
     *
     * @return View
     */
    public function edit(Device $device): View
    {
        $photoNames = Storage::disk('public')->allFiles($device->path_to_photos);
        $view = [
            'countPhoto' => count($photoNames),
            'countVideo' => $device->videos()->count(),
            'brandsAssoc' => $this->brandRepo->getIdAndNameAssoc(),
            'typesAssoc' => $this->typeRepo->getIdAndNameAssoc(),
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Девайс #' . $device->id,
        ];

        return view('admin.device.edit', compact(['view', 'device']));
    }

    /**
     * @param int $id
     * @param AdminDeviceRequest $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, AdminDeviceRequest $request): RedirectResponse
    {
        $request['is_published'] = isset($request['is_published']) ? 1 : 0;
        $request['last_modified'] = Carbon::now();
        $device = $this->deviceRepo->findOrFail($id);
        $device->update($request->all());

        return redirect()->route('admin.device.index');
    }

    /**
     * @param Device $device
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Device $device): RedirectResponse
    {
        Storage::disk('public')->deleteDirectory($device->path_to_photos);
        $device->delete();

        return redirect()->route('admin.device.index');
    }
}
