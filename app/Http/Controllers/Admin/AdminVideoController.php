<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminVideoRequest;
use App\Models\Video;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\VideoRepository;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AdminVideoController extends Controller
{
    private $deviceRepo;

    private $videoRepo;

    private $reviewRepo;

    public function __construct(
        DeviceRepository $deviceRepo,
        VideoRepository $videoRepo,
        ReviewRepository $reviewRepo
    ) {
        $this->deviceRepo = $deviceRepo;
        $this->videoRepo = $videoRepo;
        $this->reviewRepo = $reviewRepo;
    }

    //Список видео.
    public function index(int $deviceId): View
    {
        $view = [
            'deviceId' => $deviceId,
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Видео',
        ];
        $videos = $this->videoRepo->getListByDeviceId($deviceId);

        return view('admin.video.index', compact(['videos', 'view']));
    }

    //Форма создания видео.
    public function create(int $deviceId): View
    {
        $view = [
            'deviceId' => $deviceId,
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Новое видео',
        ];

        return view('admin.video.create', compact('view'));
    }

    /**
     * @param AdminVideoRequest $request
     *
     * @return RedirectResponse
     */
    public function store(AdminVideoRequest $request): RedirectResponse
    {
        $input = $request->all();

        $newVideo = new Video([
            'device_id' => $input['device_id'],
            'source' => $input['source'],
        ]);
        $newVideo->save();

        return redirect()->route('admin.video.index', [$newVideo->device_id]);
    }

    //Форма редактирования видео.
    public function edit(int $deviceId, int $videoId): View
    {
        $video = $this->videoRepo->findOrFail($videoId);
        $view = [
            'deviceId' => $deviceId,
            'devicesCount' => $this->deviceRepo->getCount(),
            'reviewsCount' => $this->reviewRepo->getCounts(),
            'title' => 'Видео #' . $videoId,
            'video' => $video,
            'videoId' => $videoId,
        ];

        return view('admin.video.edit', compact('view'));
    }

    /**
     * @param int $deviceId
     * @param int $videoId
     *
     * @param AdminVideoRequest $request
     *
     * @return RedirectResponse
     */
    public function update(int $deviceId, int $videoId, AdminVideoRequest $request): RedirectResponse
    {
        $video = $this->videoRepo->findOrFail($videoId);
        $video->update($request->all());

        return redirect()->route('admin.video.index', $deviceId);
    }

    /**
     * @param int $deviceId
     * @param int $videoId
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(int $deviceId, int $videoId): RedirectResponse
    {
        $this->videoRepo->find($videoId)->delete();

        return redirect()->route('admin.video.index', $deviceId);
    }
}
