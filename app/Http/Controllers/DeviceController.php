<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\Brand;
use App\Models\Type;
use App\Repositories\BrandRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\RatingRepository;
use App\Repositories\ReviewRepository;
use App\Repositories\TypeRepository;
use App\Repositories\VideoRepository;
use App\Services\CharGroupsToDeviceLoader;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class DeviceController extends Controller
{
    private $typeRepo;

    private $brandRepo;

    private $deviceRepo;

    private $videoRepo;

    private $reviewRepo;

    private $ratingRepo;

    private $helper;

    /**
     * @var CharGroupsToDeviceLoader
     */
    private $charGroupsToDeviceLoader;

    public function __construct(
        CharGroupsToDeviceLoader $charGroupsToDeviceLoader,
        TypeRepository $typeRepo,
        BrandRepository $brandRepo,
        DeviceRepository $deviceRepo,
        VideoRepository $videoRepo,
        ReviewRepository $reviewRepo,
        RatingRepository $ratingRepo,
        Helper $helper
    ) {
        $this->charGroupsToDeviceLoader = $charGroupsToDeviceLoader;
        $this->typeRepo = $typeRepo;
        $this->brandRepo = $brandRepo;
        $this->deviceRepo = $deviceRepo;
        $this->videoRepo = $videoRepo;
        $this->reviewRepo = $reviewRepo;
        $this->ratingRepo = $ratingRepo;
        $this->helper = $helper;
    }

    /**
     * @param Type $deviceType
     * @param string $brandSlug
     * @param string $deviceSlug
     *
     * @return View
     */
    public function __invoke(Type $deviceType, string $brandSlug, string $deviceSlug): View
    {
        $types = $this->typeRepo->getList();
        $brands = $this->brandRepo->getListByTypeId($deviceType->id);
        $currentBrand = $this->getCurrentBrandFromBrands($brands, $brandSlug);
        if (!$currentBrand) {
            abort(404);
        }
        $devices = $this->deviceRepo->getAllByTypeIdAndBrandId($deviceType->id, $currentBrand->id);
        $deviceId = $this->getCurrentDeviceIdFromDevices($devices, $deviceSlug);
        if (!$deviceId) {
            abort(404);
        }

        //Сайдбар меню.
        foreach ($devices as $device) {
            $device->isCurrent = ($device->id === $deviceId) ? true : false;
        }
        foreach ($brands as $brand) {
            $brand->devicesList = ($brand->id === $currentBrand->id) ? $devices : null;
        }
        foreach ($types as $type) {
            $type->brands = ($type->id === $deviceType->id) ? $brands : null;
        }

        $currentDevice = $this->deviceRepo->find($deviceId);
        $photoNames = Storage::disk('public')->allFiles($currentDevice->path_to_photos);
        $photos = [];
        foreach ($photoNames as $photoName) {
            $photos[] = Storage::disk('public')->url($photoName);
        }
        $this->charGroupsToDeviceLoader->load($currentDevice);
        $questions = $deviceType->questions;
        $reviews = $currentDevice->reviews()->published()->get();
        if (count($reviews)) {
            $reviewIds = [];
            foreach ($reviews as $review) {
                $reviewIds[] = $review->id;
            }
            $averageRating = $this->ratingRepo->getQuestionIdsAndAverageRatingAssoc($reviewIds);
            foreach ($questions as $question) {
                $question->averageRating = round((float) $averageRating[$question->id], 1);
                $question->cssWidth = (int) ($averageRating[$question->id] * 10 * 5);
            }
            //Общая оценка.
            $commonRating = 0.0;
            foreach ($questions as $question) {
                $commonRating += $question->averageRating;
            }
            $commonRating = round($commonRating / count($questions), 1);
        }

        $view = [
            'commonRating' => isset($commonRating) ? $commonRating : null,
            'brand' => $currentBrand,
            'breadCrumbs' => [
                0 => [
                    'name' => 'Главная',
                    'href' => '/',
                ],
                1 => [
                    'name' => $deviceType->name_plural,
                    'href' => '/' . $deviceType->slug,
                ],
                2 => [
                    'name' => $currentBrand->name,
                    'href' => '/' . $deviceType->slug . '/' . $currentBrand->slug,
                ],
                3 => [
                    'name' => $currentDevice->name,
                    'href' => '',
                ],
            ],
            'device' => $currentDevice,
            'questions' => $questions,
            'reviews' => $reviews,
            'reviewsCountWordForm' => $this->helper->getWordForm(count($reviews), ['отзыва', 'отзывов', 'отзывов']),
            'seo' => [
                'canonical' => '/' . $deviceType->slug . '/' . $currentBrand->slug . '/' . $currentDevice->slug,
                'description' => 'Мнения владельцев о ' . $deviceType->name_prepositional . ' ' .
                    $currentBrand->name . ' ' . $currentDevice->name . '. Реальные отзывы, оценки, ' .
                    'характеристики, фото, видео.',
                'keywords' => mb_strtolower($deviceType->name_singular) . ', ' . strtolower($currentBrand->name) .
                    ', ' . strtolower($currentDevice->name) . ', отзывы, оценка, мнения, написать отзыв, ' .
                    'технические характеристики, фото, видео, обзор',
                'title' => 'Отзывы о ' . $deviceType->name_prepositional . ' ' . $currentBrand->name . ' ' .
                    $currentDevice->name,
            ],
            'sidebarMenu' => $types,
            'type' => $deviceType,
            'videos' => $this->videoRepo->getListByDeviceId($currentDevice->id),
        ];

        return view('device.index', compact(['photos', 'view']));
    }

    /**
     * @param Collection|Brand[] $brands
     * @param string $slug
     *
     * @return Brand|null
     *
     * @TODO Вынести в трейт или что-то другое
     */
    private function getCurrentBrandFromBrands(Collection $brands, string $slug): ?Brand
    {
        foreach ($brands as $brand) {
            if ($slug === $brand->slug) {
                return $brand;
            }
        }

        return null;
    }

    //TODO php 7.2. Написать, что возвращает метод (?int).
    private function getCurrentDeviceIdFromDevices(Collection $devices, string $slug)
    {
        foreach ($devices as $device) {
            if ($slug === $device->slug) {
                return $device->id;
            }
        }

        return null;
    }
}
