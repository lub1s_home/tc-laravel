<?php

namespace App\Http\Controllers;

use App\Entities\DevicesList;
use App\Models\Type;
use App\Repositories\BrandRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\TypeRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;

class BrandController extends Controller
{
    private $typeRepo;

    private $brandRepo;

    private $deviceRepo;

    private $devicesList;

    public function __construct(
        TypeRepository $typeRepo,
        BrandRepository $brandRepo,
        DeviceRepository $deviceRepo,
        DevicesList $devicesList
    ) {
        $this->typeRepo = $typeRepo;
        $this->brandRepo = $brandRepo;
        $this->deviceRepo = $deviceRepo;
        $this->devicesList = $devicesList;
    }

    /**
     * @param Type $deviceType
     * @param string $brandSlug
     *
     * @return View
     */
    public function __invoke(Type $deviceType, string $brandSlug): View
    {
        $types = $this->typeRepo->getList();
        $brands = $this->brandRepo->getListByTypeId($deviceType->id);
        $currentBrand = $this->getCurrentBrandFromBrands($brands, $brandSlug);
        if (!$currentBrand) {
            abort(404);
        }
        $devices = $this->deviceRepo->getListByTypeIdAndBrandId($deviceType->id, $currentBrand->id);

        //Сайдбар меню.
        foreach ($brands as $brand) {
            //TODO будет бага, если девайсов больше 10, то в сайдбар попадут только первые 10.
            $brand->devicesList = ($brand->id === $currentBrand->id) ? $devices : null;
        }
        foreach ($types as $type) {
            $type->brands = ($type->id === $deviceType->id) ? $brands : null;
        }

        //Список девайсов.
        $this->devicesList->upgrade($devices);
        $devicesCount = $this->deviceRepo->getCountByTypeIdAndBrandId($deviceType->id, $currentBrand->id);
        $button = $this->devicesList->getButton($devicesCount, $deviceType->id, $currentBrand->id);

        $view = [
            'breadCrumbs' => [
                [
                    'name' => 'Главная',
                    'href' => '/',
                ],
                [
                    'name' => $deviceType->name_plural,
                    'href' => '/' . $deviceType->slug,
                ],
                [
                    'name' => $currentBrand->name,
                    'href' => '',
                ],
            ],
            'type' => $deviceType,
            'devicesList' => [
                'h1' => $deviceType->name_plural . ' ' . $currentBrand->name  . ' (' . $devicesCount . ' шт)',
                'sortHref' => '/api/ajax?type_id=' . $deviceType->id . '&brand_id=' . $currentBrand->id . '&page=1',
                'button' => $button,
                'items' => $devices,
            ],
            'searchCta' => [
                'href' => '?device_type=1&brand=1',
            ],
            'seo' => [
                'canonical' => '/' . $deviceType->slug . '/' . $currentBrand->slug,
                'description' => $deviceType->name_plural . ' ' . $currentBrand->name . ': реальные отзывы, ' .
                    'оценки, технические характеристики, обзоры, мнения владельцев, фото, видео.',
                'keywords' => mb_strtolower($deviceType->name_plural) . ', ' . strtolower($currentBrand->name) .
                    ', отзывы, мнения, новинки, фото, видео, обзоры, технические характеристики',
                'title' => $deviceType->name_plural . ' ' . $currentBrand->name . ' - отзывы',
            ],
            'sidebarMenu' => $types,
        ];

        return view('brand.index', compact('view'));
    }

    //TODO php 7.2. Написать, что возвращает метод (?\App\Model\Brand).
    //TODO вынести в трейт или что-то другое.
    private function getCurrentBrandFromBrands(Collection $brands, string $slug)
    {
        foreach ($brands as $brand) {
            if ($slug === $brand->slug) {
                return $brand;
            }
        }

        return null;
    }
}
