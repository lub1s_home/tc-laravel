<?php

namespace App\Http\Controllers;

use App\Entities\DevicesList;
use App\Http\Requests\DevicesRequest;
use App\Repositories\DeviceRepository;
use Illuminate\Http\JsonResponse;

class AjaxController extends Controller
{
    private $deviceRepo;

    private $devicesList;

    public function __construct(DeviceRepository $deviceRepo, DevicesList $devicesList)
    {
        $this->deviceRepo = $deviceRepo;
        $this->devicesList = $devicesList;
    }

    /**
     * @param DevicesRequest $req
     *
     * @return JsonResponse
     */
    public function devicesList(DevicesRequest $req): JsonResponse
    {
        $devices = $req->brand_id
            ? $this->deviceRepo->getListByTypeIdAndBrandId($req->type_id, $req->brand_id, $req->page, $req->sort)
            : $this->deviceRepo->getListByTypeId($req->type_id, $req->page, $req->sort);
        if (!count($devices)) {
            die('Нет страницы с девайсами.');
        }
        $this->devicesList->upgrade($devices);
        $view = [
            'devicesList' => [
                'items' => $devices,
            ],
        ];
        $html = (string) view('include.devicesList', compact('view'));
        $devicesCount = $req->brand_id
            ? $this->deviceRepo->getCountByTypeIdAndBrandId($req->type_id, $req->brand_id)
            : $this->deviceRepo->getCountByTypeId($req->type_id);
        $button = $this->devicesList->getButton($devicesCount, $req->type_id, $req->brand_id, $req->page, $req->sort);

        return response()->json(compact(['html', 'button']));
    }
}
