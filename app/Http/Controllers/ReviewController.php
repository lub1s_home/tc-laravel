<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReviewRequest;
use App\Models\Rating;
use App\Models\Review;
use App\Repositories\DeviceRepository;
use App\Repositories\QuestionRepository;

class ReviewController extends Controller
{
    private $deviceRepo;

    private $questionRepo;

    public function __construct(DeviceRepository $deviceRepo, QuestionRepository $questionRepo)
    {
        $this->deviceRepo = $deviceRepo;
        $this->questionRepo = $questionRepo;
    }

    //Создание отзыва.
    public function store(CreateReviewRequest $request)
    {
        $input = $request->all();
        $device = $this->deviceRepo->findOrFail($input['device_id']);
        $requiredQuestionIds = $this->questionRepo->getIdsByTypeId($device->type_id);
        foreach ($requiredQuestionIds as $questionId) {
            if (!isset($input['rating'][$questionId]) || !in_array($input['rating'][$questionId], [1, 2, 3, 4, 5])) {
                die('Оценка не валидна');
            }
        }

        $review = new Review([
            'device_id' => $input['device_id'],
            'name' => $input['name'],
            'is_published' => 0,
            'review' => $input['review'],
        ]);
        $review->save();
        foreach ($requiredQuestionIds as $questionId) {
            $review->ratings()->save(new Rating([
                'question_id' => $questionId,
                'value' => $input['rating'][$questionId],
            ]));
        }
        \Session::flash('review_sent', 'Спасибо, отзыв отправлен.');

        return redirect($_SERVER['HTTP_REFERER'] . '#review-add');
    }
}
