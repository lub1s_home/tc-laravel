<?php

namespace App\Entities;

use App\Helper;
use App\Repositories\DeviceRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class DevicesList
{
    private $deviceRepo;

    private $reviewRepo;

    private $helper;

    public function __construct(
        DeviceRepository $deviceRepo,
        ReviewRepository $reviewRepo,
        Helper $helper
    ) {
        $this->deviceRepo = $deviceRepo;
        $this->reviewRepo = $reviewRepo;
        $this->helper = $helper;
    }

    public function upgrade(Collection $devices)
    {
        $deviceIds = [];
        foreach ($devices as $device) {
            $deviceIds[] = $device->id;
        }
        $reviewsCount = $this->reviewRepo->getDeviceIdsAndReviewsCountAssoc($deviceIds);
        $averageRating = $this->reviewRepo->getDeviceIdsAndAverageRatingAssoc($deviceIds);
        $lastReviews = $this->reviewRepo->getDeviceIdsAndLastReviewsAssoc($deviceIds);
        foreach ($devices as $device) {
            if (isset($reviewsCount[$device->id])) {
                $device->reviewsCount = $reviewsCount[$device->id];
                $device->fReviewsCount =
                    $this->helper->getWordForm($reviewsCount[$device->id], ['отзыва', 'отзывов', 'отзывов']);
            }
            if (isset($averageRating[$device->id])) {
                $device->averageRating = round((float) $averageRating[$device->id], 1);
                $device->cssWidth = (int) ($averageRating[$device->id] * 10 * 5);
            }
            if (isset($lastReviews[$device->id])) {
                $lastReviews[$device->id]->formattedContent = mb_substr($lastReviews[$device->id]->review, 0, 150) .
                    '...';
                $device->lastReview = $lastReviews[$device->id];
            }
            $photos = Storage::disk('public')->files($device->path_to_photos);
            $device->photoSrc = isset($photos[0]) ? Storage::disk('public')->url($photos[0]) : '#';
            $device->setAttribute('href', '/' . $device->type->slug . '/' . $device->brand->slug . '/' . $device->slug);
        }
    }

    public function getButton(
        int $devicesCount,
        int $typeId,
        int $brandId = null,
        int $page = 1,
        int $sort = DeviceRepository::SORT_BY_NAME
    ): array {
        $areShown = $this->deviceRepo::PAGE_SIZE * $page;
        $leftToShow = $devicesCount - $areShown;
        $showMore = $leftToShow < $this->deviceRepo::PAGE_SIZE
            ? $leftToShow
            : $this->deviceRepo::PAGE_SIZE;

        return [
            'toDisplay' => $devicesCount > $areShown ? true : false,
            'text' => 'Еще ' . $showMore . ' из ' . $leftToShow,
            'href' => '/api/ajax?type_id=' . $typeId .
                ($brandId ? '&brand_id=' . $brandId : '') .
                '&page=' . ($page + 1) .
                '&sort=' . $sort,
        ];
    }
}
