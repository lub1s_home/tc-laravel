<?php

namespace App\Console\Commands;

use App\Models\Device;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ConvertTimestampToDatetime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:convert-timestamp-to-datetime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert from timestamp to datetime in Devices table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $devices = Device::all();

        foreach ($devices as $device) {
            if ($device->lastModified) {
                $device->last_modified = Carbon::createFromTimestamp($device->lastModified);
            }
            if ($device->releaseDate) {
                $device->release_date = Carbon::createFromTimestamp($device->releaseDate);
            }
            if ($device->lastModified || $device->releaseDate) {
                $device->save();
            }
        }
    }
}
