<?php

namespace App\Repositories;

use App\Models\Device;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DeviceRepository extends AbstractRepository
{
    const PAGE_SIZE = 10;

    const SORT_BY_NAME = 1;
    const SORT_BY_RELEASE_DATE = 2;
    const SORT_BY_REVIEWS_COUNT = 3;
    //TODO Добавить сортировку по качеству.

    public function __construct(Device $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     *
     * @return Device
     */
    public function find(int $id): Device
    {
        return $this->model->find($id);
    }

    /**
     * @param int $typeId
     *
     * @return int
     */
    public function getCountByTypeId(int $typeId): int
    {
        return $this->model->whereTypeId($typeId)->published()->count();
    }

    /**
     * @param int $typeId
     * @param int $brandId
     *
     * @return int
     */
    public function getCountByTypeIdAndBrandId(int $typeId, int $brandId): int
    {
        return $this->model->whereTypeId($typeId)->whereBrandId($brandId)->published()->count();
    }

    /**
     * @return Collection|Device[]
     */
    public function getWithTypeAndBrand(): Collection
    {
        return $this->model->with(['brand', 'type'])->latest('id')->get();
    }

    public function getAllByTypeIdAndBrandId(int $typeId, int $brandId): Collection
    {
        $devices = $this->model->select(['id', 'name'])
            ->where('type_id', '=', $typeId)
            ->where('brand_id', '=', $brandId)
            ->oldest('name')
            ->get();

        return $devices;
    }

    /**
     * @param int $typeId
     * @param int $page
     * @param int $sort
     *
     * @return Collection
     */
    public function getListByTypeId(int $typeId, int $page = 1, int $sort = self::SORT_BY_NAME): Collection
    {
        $devices = $this->model
            ->with(['brand', 'type'])
            ->whereTypeId($typeId)
            ->published()
            ->limit(self::PAGE_SIZE)
            ->offset($this->getOffset($page));

        $this->addSorting($devices, $sort);

        return $devices->get();
    }

    /**
     * @param int $typeId
     * @param int $brandId
     * @param int $page
     * @param int $sort
     *
     * @return Collection
     */
    public function getListByTypeIdAndBrandId(
        int $typeId,
        int $brandId,
        int $page = 1,
        int $sort = self::SORT_BY_NAME
    ): Collection {
        $devices = $this->model
            ->with(['brand', 'type'])
            ->whereTypeId($typeId)
            ->whereBrandId($brandId)
            ->published()
            ->limit(self::PAGE_SIZE)
            ->offset($this->getOffset($page));

        $this->addSorting($devices, $sort);

        return $devices->get();
    }

    public function getTypeIdsAndBrandsCountAssoc(): array
    {
        $assoc = $this->model->select([
            'devices.type_id',
            DB::raw('count(distinct devices.brand_id) as brandsCount'),
        ])
            ->published()
            ->groupBy('devices.type_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'brandsCount', 'type_id');
    }

    public function getTypeIdsAndDevicesCountAssoc(): array
    {
        $assoc = $this->model->select([
            'devices.type_id',
            DB::raw('count(devices.id) as devicesCount'),
        ])
            ->published()
            ->groupBy('devices.type_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'devicesCount', 'type_id');
    }

    public function getTypeIdsAndReviewsCountAssoc(): array
    {
        $assoc = $this->model->select([
            'devices.type_id',
            DB::raw('count(reviews.id) as reviewsCount'),
        ])
            ->join('reviews', 'devices.id', '=', 'reviews.device_id')
            ->where('reviews.is_published', true)
            ->published()
            ->groupBy('devices.type_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'reviewsCount', 'type_id');
    }

    public function getBrandIdsAndDevicesCountAssoc(int $typeId): array
    {
        $assoc = $this->model->select([
            'devices.brand_id',
            DB::raw('count(devices.id) as devicesCount'),
        ])
            ->where('devices.type_id', '=', $typeId)
            ->published()
            ->groupBy('devices.brand_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'devicesCount', 'brand_id');
    }

    public function getBrandIdsAndReviewsCountAssoc(int $typeId): array
    {
        $assoc = $this->model->select([
            'devices.brand_id',
            DB::raw('count(reviews.id) as reviewsCount'),
        ])
            ->join('reviews', 'reviews.device_id', '=', 'devices.id')
            ->where('devices.type_id', '=', $typeId)
            ->where('reviews.is_published', '=', true)
            ->published()
            ->groupBy('devices.brand_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'reviewsCount', 'brand_id');
    }

    private function addSorting(Builder $builder, int $sorting)
    {
        switch ($sorting) {
            case self::SORT_BY_NAME:
                $builder->oldest('devices.name');
                break;
            case self::SORT_BY_RELEASE_DATE:
                $builder->latest('devices.release_date');
                break;
            case self::SORT_BY_REVIEWS_COUNT:
                $builder
                    ->select('devices.*')
                    ->addSelect(DB::raw('count(reviews.id) as reviewsCount'))
                    ->leftJoin('reviews', 'devices.id', '=', 'reviews.device_id')
                    ->groupBy([
                        'devices.id',
                        'devices.type_id',
                        'devices.brand_id',
                        'devices.name',
                        'devices.last_modified',
                        'devices.release_date',
                        'devices.is_published',
                    ])
                    ->latest('reviewsCount');
        }
    }

    /**
     * @param int|null $pageNumber
     *
     * @return int
     */
    private function getOffset(?int $pageNumber): int
    {
        if (!$pageNumber) {
            $pageNumber = 1;
        }

        return self::PAGE_SIZE * ($pageNumber - 1);
    }
}
