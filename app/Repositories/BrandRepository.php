<?php

namespace App\Repositories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Collection;

class BrandRepository extends AbstractRepository
{
    public function __construct(Brand $model)
    {
        $this->model = $model;
    }

    public function getListByTypeId(int $typeId): Collection
    {
        $brands = $this->model->select(['brands.id', 'brands.name'])
            ->where('devices.type_id', '=', $typeId)
            ->where('devices.is_published', '=', true)
            ->distinct()
            ->join('devices', 'brands.id', '=', 'devices.brand_id')
            ->oldest('brands.name')
            ->get();

        return $brands;
    }

    public function getIdAndNameAssoc(): array
    {
        $brands = $this->model->select('id', 'name')
            ->oldest('name')
            ->get()
            ->toArray();

        return array_column($brands, 'name', 'id');
    }
}
