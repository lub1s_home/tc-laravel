<?php

namespace App\Repositories;

use App\Models\Rating;
use Illuminate\Support\Facades\DB;

class RatingRepository extends AbstractRepository
{
    public function __construct(Rating $model)
    {
        $this->model = $model;
    }

    public function getQuestionIdsAndAverageRatingAssoc(array $reviewIds): array
    {
        $assoc = $this->model->select([
            'question_id',
            DB::raw('avg(value) as averageRating')
        ])
            ->whereIn('review_id', $reviewIds)
            ->groupBy('question_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'averageRating', 'question_id');
    }
}
