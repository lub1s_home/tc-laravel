<?php

namespace App\Repositories;

use App\Models\Type;
use Illuminate\Database\Eloquent\Collection;

class TypeRepository extends AbstractRepository
{
    public function __construct(Type $model)
    {
        $this->model = $model;
    }

    public function getIdAndNameAssoc(): array
    {
        $types = $this->model->select(['id', 'name_singular'])
            ->oldest('name_singular')
            ->get()
            ->toArray();

        return array_column($types, 'name_singular', 'id');
    }

    /**
     * @return Collection|Type[]
     */
    public function getList(): Collection
    {
        return $this->model->orderBy('name_plural')->get();
    }
}
