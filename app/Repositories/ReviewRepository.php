<?php

namespace App\Repositories;

use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ReviewRepository extends AbstractRepository
{
    public function __construct(Review $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     *
     * @return Review
     */
    public function find(int $id): Review
    {
        return $this->model->find($id);
    }

    public function getCounts(): array
    {
        return [
            'all' =>$this->model->count(),
            'isNotPublished' => $this->model->where('reviews.is_published', '=', false)->count(),
        ];
    }

    public function getList(): Collection
    {
        $reviews = $this->model->latest('id')->get();
        foreach ($reviews as $review) {
            $review->formattedCreatedAt = $this->getFormattedDate($review->created_at);
            $review->formattedReview = mb_substr($review->review, 0, 50) . '...';
        }

        return $reviews;
    }

    public function getDeviceIdsAndLastReviewsAssoc(array $deviceIds): array
    {
        $reviewIds = $this->model->select([DB::raw('max(id) as reviewId')])
            ->whereIn('device_id', $deviceIds)
            ->where('is_published', '=', true)
            ->groupBy('device_id')
            ->get()
            ->toArray();
        $reviewIds = array_column($reviewIds, 'reviewId');

        $reviews = $this->model->whereIn('id', $reviewIds)->get();
        $result = [];
        foreach ($reviews as $review) {
            $review->formattedCreatedAt = $this->getFormattedDate($review->created_at);
            $result[$review->device_id] = $review;
        }

        return $result;
    }

    public function getDeviceIdsAndReviewsCountAssoc(array $deviceIds): array
    {
        $assoc = $this->model->select([
            'device_id',
            DB::raw('count(id) as reviewCount')
        ])
            ->whereIn('device_id', $deviceIds)
            ->where('is_published', '=', 1)
            ->groupBy('device_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'reviewCount', 'device_id');
    }

    public function getDeviceIdsAndAverageRatingAssoc(array $deviceIds): array
    {
        $assoc = $this->model->select([
            'reviews.device_id',
            DB::raw('avg(ratings.value) as averageRating')
        ])
            ->join('ratings', 'ratings.review_id', '=', 'reviews.id')
            ->whereIn('reviews.device_id', $deviceIds)
            ->groupBy('reviews.device_id')
            ->get()
            ->toArray();

        return array_column($assoc, 'averageRating', 'device_id');
    }

    private function getFormattedDate(Carbon $carbon): string
    {
        $months = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];
        $month = $months[$carbon->month];

        return $carbon->formatLocalized("%d $month %Y - %H:%M");
    }
}
