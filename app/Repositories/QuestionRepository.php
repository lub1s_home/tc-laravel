<?php

namespace App\Repositories;

use App\Models\Question;
use Illuminate\Database\Eloquent\Collection;

class QuestionRepository extends AbstractRepository
{
    public function __construct(Question $model)
    {
        $this->model = $model;
    }

    public function getIdsByTypeId(int $typeId): array
    {
        $array = $this->model->select(['id'])
            ->where('type_id', '=', $typeId)
            ->get()
            ->toArray();

        return array_column($array, 'id');
    }
}
