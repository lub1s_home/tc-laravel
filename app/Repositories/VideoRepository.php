<?php

namespace App\Repositories;

use App\Models\Video;
use Illuminate\Database\Eloquent\Collection;

class VideoRepository extends AbstractRepository
{
    public function __construct(Video $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $id
     *
     * @return Video
     */
    public function find(int $id): Video
    {
        return $this->model->find($id);
    }

    /**
     * @param int $deviceId
     *
     * @return Collection
     */
    public function getListByDeviceId(int $deviceId): Collection
    {
        return $this->model->whereDeviceId($deviceId)->latest('id')->get();
    }
}
