<?php

namespace App\Repositories\Devices;

use App\Models\Chars\DeviceChar;
use Illuminate\Support\Facades\DB;

class DeviceCharRepository
{
    /**
     * @param array|int[] $charIds
     *
     * @return array
     */
    public function getCharIdAndMaxMinValueMap(array $charIds): array
    {
        $res = DeviceChar::select(DB::raw('`char_id`, max(`value`) as max_value, min(`value`) as min_value'))
            ->whereIn('char_id', $charIds)
            ->groupBy('char_id')
            ->get()
            ->toArray();

        return array_column($res, null, 'char_id');
    }
}
