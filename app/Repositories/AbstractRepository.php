<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
    /**
     * @var Model
     */
    protected $model;

    public function getCount(): int
    {
        return $this->model->count();
    }

    public function findOrFail(int $id): Model
    {
        return $this->model->findOrFail($id);
    }
}
