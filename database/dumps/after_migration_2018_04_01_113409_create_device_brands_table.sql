-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 01 2018 г., 14:07
-- Версия сервера: 5.5.52-38.3
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cp63634_techbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `device_brands`
--

CREATE TABLE IF NOT EXISTS `device_brands` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `site` varchar(50) NOT NULL,
  `wordstat` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wordstat` (`wordstat`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `device_brands`
--

INSERT INTO `device_brands` (`id`, `name`, `site_url`, `wordstat`) VALUES
(1, 'Defender', 'http://defender.ru', 195798),
(2, 'Akai professional', 'http://www.akaimusic.ru', 41399),
(3, 'Molecula', '', 6755),
(4, 'LG', 'http://www.lg.com', 3293745),
(5, 'Genius', 'http://www.geniusnet.com', 170611),
(6, 'Razer', 'https://www.razerzone.ru', 171013),
(7, 'Kingston', 'http://www.kingston.com', 128323),
(8, 'Logitech', 'http://gaming.logitech.com', 295735),
(9, 'Speedlink', 'https://www.speedlink.ru', 12077),
(10, 'Pioneer', 'http://www.pioneer-rus.ru', 495139),
(11, 'Microlab', 'http://www.microlab.com/?r=russian', 52090),
(12, 'Qcyber', 'http://q-cyber.com', 9307),
(13, 'A4Tech', 'http://www.a4tech.ru', 133155),
(14, 'Microsoft', 'https://www.microsoft.com', 1375859),
(15, 'Philips', 'http://www.philips.ru', 1308103),
(16, 'DNS', 'http://www.dns-shop.ru', 1063419),
(17, 'Fujimi', 'http://www.fujimishop.ru', 3942),
(18, 'Hewlett Packard (HP)', 'http://www8.hp.com/ru/ru/home.html', 16823),
(19, 'Sven', 'http://www.sven.fi/ru', 155211),
(20, 'no-brand', '', 0),
(21, 'Beats Electronics', 'http://ru.beatsbydre.com', 280419),
(22, 'Dexp', 'http://dexp.club', 431036),
(23, 'MSI', 'https://ru.msi.com', 640862),
(24, 'SJCAM', 'http://sjcam.ru', 39025),
(25, 'Shini', '', 9167),
(26, 'Ipega', 'http://www.ipega.hk/index.php?lang=en', 2308),
(27, 'Boya', 'http://www.boya-mic.com', 4277),
(28, 'Xiaomi', 'http://ru-mi.com', 2192005),
(29, 'Bluedio', '', 9610),
(30, 'Marvo', '', 6276),
(31, 'GoPro', 'http://gopro.ru', 179082),
(32, 'Elephone', 'http://www.elephonestore.com', 39064),
(33, 'Oukitel', 'http://www.oukitel.com', 109683),
(34, 'Homtom', 'http://www.homtom.cc', 104489),
(35, 'SmartBuy', 'http://smartbuy-russia.ru', 48384),
(36, 'Blue Microphones', 'http://www.bluemics.ru', 862),
(37, 'GitUp', '', 1547),
(38, 'Fly', 'http://www.fly-phone.ru', 967796),
(39, 'Prestigio', 'http://www.prestigio.ru', 470641),
(40, 'Impression Electronics', 'http://impression.ua', 630),
(41, 'Lenovo', 'http://www.lenovo.com', 2691620),
(42, 'Micromax', 'https://micromaxstore.ru', 230229),
(43, 'Tele2', 'http://tele2.ru', 317570),
(44, 'Gemix', 'http://www.gemix.ua', 6645),
(45, 'Digma', 'http://www.digma.ru', 171142),
(46, 'Gembird', 'http://www.gembird.ru', 28755),
(47, 'Audio-technica', 'http://www.audio-technica.ru', 41217),
(48, 'Blackview', 'http://blackview.pro', 117559),
(49, 'Asus', 'https://www.asus.com/ru', 3174249),
(50, 'Sony', '', 3261587),
(51, 'Sigma mobile', 'http://sigmamobile.net/ru', 148827),
(52, 'Creative', 'http://ru.creative.com', 125580),
(53, 'Samsung', 'http://www.samsung.com', 7090052),
(54, 'Umi', 'http://www.umidigi.com', 104481),
(55, 'Acer', 'http://www.acer.com', 1097212),
(56, 'HTC', 'http://www.htc.com', 1276388),
(57, 'Arduino', 'http://arduino.ru', 182641),
(58, 'Thermaltake', 'http://www.thermaltakerussia.ru', 48210),
(59, 'DeepCool', 'http://www.deepcool.com/ru', 52871),
(60, 'AOC', '', 47449),
(61, 'Mpie', '', 1949),
(62, 'Apple', '', 1295043),
(63, 'ZTE', 'http://www.zte.ru', 1075941),
(64, 'Canon', '', 1649110),
(65, 'Cubot', 'http://ru.cubot.net', 49261),
(66, 'Doogee', 'http://www.doogee.cc', 219585),
(67, 'Bluboo', 'http://bluboo.hk', 37304),
(68, 'Sades', '', 25850),
(69, 'Kinbas', '', 123),
(70, 'Kangling', '', 109),
(71, 'Sapphire', 'http://www.sapphiretech.com', 88810),
(73, 'Bravis', '', 48280),
(74, 'Ausdom', 'http://www.ausdom.com', 836),
(75, 'Leagoo', 'http://www.leagoo.com', 37725),
(76, 'Intex', '', 72483),
(77, 'Vkworld', 'http://www.vkworld.cc', 18305),
(78, 'UleFone', 'http://ulefone.com/ru', 43053),
(79, 'Cosonic', '', 2055),
(80, 'Meizu', 'http://mymeizu.ru', 1092281),
(81, 'AGM', 'http://www.agmmobile.com/ru', 25147),
(82, 'Zuk', 'http://www.zukmobile.cc', 38649),
(83, 'Red Square', 'http://red-square.org', 24971),
(84, 'Gigabyte', 'http://ru.gigabyte.com', 414491),
(85, 'MantisTek', '', 226),
(86, 'Onda', 'http://onda.com.ru', 24911),
(87, 'Vernee', 'http://www.vernee.cc', 20144),
(88, 'New bee', '', 235),
(89, 'Trinity', 'https://trinity-audio-engineering.myshopify.com', 33395),
(90, 'Meze', 'https://www.mezeheadphones.com', 1376),
(91, 'Roccat', 'http://www.roccat.org', 19830),
(92, 'Ducky Channel', 'http://www.duckychannel.com.tw', 11),
(93, 'Corsair', 'http://www.corsair.com', 94000),
(94, 'SteelSeries', 'https://ru.steelseries.com', 63000),
(95, 'Nixeus', 'http://www.nixeus.com', 166),
(96, 'Mionix', 'https://mionix.net', 913),
(97, 'Cougar', 'http://www.cougar-world.ru', 34500),
(98, 'Dream Machines', 'https://www.dreammachines.pl', 1500),
(99, 'Zowie', 'http://zowie.benq.com', 18300),
(100, 'Bloody', 'http://www.bloody.com/ru', 197500),
(101, 'HyperX', 'https://www.hyperxgaming.com/ru', 77600);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
