-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 02 2018 г., 22:06
-- Версия сервера: 5.5.52-38.3
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cp63634_techbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `device_types`
--

CREATE TABLE IF NOT EXISTS `device_types` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_plural` varchar(50) NOT NULL,
  `name_singular` varchar(50) NOT NULL,
  `name_prepositional` varchar(50) NOT NULL,
  `for_link` varchar(50) NOT NULL,
  `wordstat` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wordstat` (`wordstat`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `device_types`
--

INSERT INTO `device_types` (`id`, `name_plural`, `name_singular`, `name_prepositional`, `slug`, `wordstat`) VALUES
(1, 'Наборы клавиатура + мышь', 'Набор (клавиатура + мышь)', 'наборе клавиатура + мышь', 'set_keyboard_mouse', 643),
(3, 'Компьютерные мыши', 'Компьютерная мышь', 'компьютерной мыши', 'mouse', 27610),
(4, 'Наушники', 'Наушники', 'наушниках', 'headphones', 1237493),
(5, 'Мониторы', 'Монитор', 'мониторе', 'monitor', 1177173),
(6, 'Клавиатуры', 'Клавиатура', 'клавиатуре', 'keyboard', 1565038),
(7, 'Геймпады', 'Геймпад', 'геймпаде', 'gamepad', 142589),
(8, 'Dj-контроллеры', 'Dj-контроллер', 'dj-контроллере', 'dj-controller', 4746),
(9, 'Колонки', 'Колонки', 'колонках', 'loudspeakers', 1350815),
(10, 'Микрофоны', 'Микрофон', 'микрофоне', 'microphone', 724820),
(11, 'Экшн-камеры', 'Экшн-камера', 'экшн-камере', 'action-camera', 75855),
(12, 'Смартфоны', 'Смартфон', 'смартфоне', 'smartphone', 3866993),
(13, 'Планшеты', 'Планшет', 'планшете', 'tablet', 3865580),
(15, 'Вентиляторы для ПК', 'Вентилятор для компьютера', 'вентиляторе для ПК', 'ventilator_for_computer', 14881),
(16, 'Портативные игровые приставки', 'Портативная игровая приставка', 'портативной игровой приставке', 'portable_game_console', 1848),
(18, 'Видеокарты', 'Видеокарта', 'видеокарте', 'video_card', 1306688);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
