-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: tc-laravel-local
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `char_options`
--

DROP TABLE IF EXISTS `char_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `char_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `char_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `char_options_char_id_foreign` (`char_id`),
  CONSTRAINT `char_options_char_id_foreign` FOREIGN KEY (`char_id`) REFERENCES `chars` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `char_options`
--

LOCK TABLES `char_options` WRITE;
/*!40000 ALTER TABLE `char_options` DISABLE KEYS */;
INSERT INTO `char_options` VALUES (1,5,'Hero'),(2,5,'Mercury'),(3,5,'Owl-Eye'),(4,5,'PixArt Avago 3050'),(5,5,'PixArt Avago SDNS 3059 SS'),(6,5,'PixArt PMW 3310'),(7,5,'PixArt PMW 3320'),(8,5,'PixArt PMW 3325'),(9,5,'PixArt PMW 3360'),(10,5,'PixArt PMW 3361'),(11,5,'PixArt PMW 3366'),(12,5,'PixArt PMW 3367'),(13,5,'PixArt S 3988'),(14,5,'Avago 5050'),(15,5,'PixArt Avago 9800'),(16,5,'TrueMove3'),(17,5,'Twin-Tech R1'),(18,7,'Huano'),(19,7,'Huano Black'),(20,7,'Light Strike'),(21,7,'Omron Mechanical'),(22,7,'SteelSeries'),(23,8,'для правой руки'),(24,8,'симметричная'),(25,9,'беспроводная'),(26,9,'гибридная'),(27,9,'проводная'),(28,17,'Kailh'),(29,17,'TTC'),(30,18,'Kailh'),(31,19,'Huano'),(32,19,'OTM'),(33,19,'TTC'),(34,22,'собственный Li-Ion'),(35,31,'SteelSeries QX2 Linear Mechanical RGB Switch'),(36,34,'механические'),(37,31,'Cherry MX Red'),(38,40,'скелетон'),(39,31,'Gateron'),(40,41,'Double-Shot Injection'),(41,31,'Jixian Blue'),(42,31,'Romer-G'),(43,31,'Cherry MX Brown или Cherry MX Speed'),(44,31,'Cherry MX'),(45,34,'плунжерные'),(46,34,'мембранные'),(47,41,'PBT, double shot'),(48,31,'Gateron Blue'),(49,31,'Kailh Blue');
/*!40000 ALTER TABLE `char_options` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-28 22:45:26
