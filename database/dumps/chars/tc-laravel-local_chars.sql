-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: tc-laravel-local
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chars`
--

DROP TABLE IF EXISTS `chars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned DEFAULT NULL,
  `sorting` smallint(6) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity` enum('SLIDER','RANGE','ONE_FROM','MANY_FROM','BOOL') COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chars_type_id_foreign` (`type_id`),
  CONSTRAINT `chars_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chars`
--

LOCK TABLES `chars` WRITE;
/*!40000 ALTER TABLE `chars` DISABLE KEYS */;
INSERT INTO `chars` VALUES (1,3,0,'вес','SLIDER','{\"unit\": \"г\"}'),(2,3,0,'длина','SLIDER','{\"unit\": \"см\"}'),(3,3,0,'ширина','SLIDER','{\"unit\": \"см\"}'),(4,3,0,'высота','SLIDER','{\"unit\": \"см\"}'),(5,3,0,'сенсор','ONE_FROM',''),(6,3,0,'программное обеспечение','BOOL',''),(7,3,0,'переключатели клавиш','ONE_FROM',''),(8,3,0,'форма','ONE_FROM',''),(9,3,0,'тип соединения','ONE_FROM',''),(10,3,0,'количество кнопок','SLIDER','{\"unit\": \"шт\"}'),(11,3,0,'подсветка','BOOL',''),(12,3,0,'максимальное разрешение','SLIDER','{\"unit\": \"dpi\"}'),(13,3,0,'минимальное разрешение','SLIDER','{\"unit\": \"dpi\"}'),(14,3,0,'длина кабеля','SLIDER','{\"unit\": \"м\", \"if\": {\"parentId\": \"9\", \"value\": \"проводная\"}}'),(15,3,0,'долговечность переключателей','SLIDER','{\"unit\": \"млн кликов\"}'),(16,3,0,'время отклика','SLIDER','{\"unit\": \"мс\"}'),(17,3,0,'переключатели клавиш (боковые кнопки)','ONE_FROM',''),(18,3,0,'энкодер колеса','ONE_FROM',''),(19,3,0,'переключатели клавиш (колесо)','ONE_FROM',''),(20,3,0,'максимальное ускорение','SLIDER','{\"unit\": \"G\", \"unitPrefix\": \">\"}'),(21,3,0,'максимальная скорость','SLIDER','{\"unit\": \"дюйм/с\", \"unitPrefix\": \">\"}'),(22,3,0,'источник питания','ONE_FROM','{\"options\": {\"1\": \"собственный Li-Ion\"}, \"if\": {\"parentId\": 9, \"value\": \"беспроводная\"}}'),(23,3,0,'время работы аккумулятора','SLIDER','{\"unit\": \"ч\"}'),(24,6,0,'подсветка','BOOL',''),(25,6,0,'антигостинг','BOOL',''),(26,6,0,'вес','SLIDER','{\"unit\": \"г\"}'),(27,6,0,'длина','SLIDER','{\"unit\": \"см\"}'),(28,6,0,'ширина','SLIDER','{\"unit\": \"см\"}'),(29,6,0,'высота','SLIDER','{\"unit\": \"см\"}'),(30,6,0,'длина кабеля','SLIDER','{\"unit\": \"м\"}'),(31,6,0,'переключатели клавиш','ONE_FROM','{\"if\": {\"parentId\": 34, \"value\": 36}}'),(32,6,0,'долговечность переключателей','SLIDER','{\"unit\": \"млн кликов\"}'),(33,6,0,'программное обеспечение','BOOL',''),(34,6,0,'тип клавиш','ONE_FROM',''),(35,6,0,'частота опроса','SLIDER','{\"unit\": \"Гц\"}'),(36,6,0,'время отклика','SLIDER','{\"unit\": \"мс\"}'),(37,6,0,'блокировка клавиши Windows','BOOL',''),(38,6,0,'NUM-pad','BOOL',''),(39,6,0,'мультимедийные клавиши','BOOL',''),(40,6,0,'дизайн','ONE_FROM',''),(41,6,0,'кейкапы','ONE_FROM',''),(42,6,0,'съемный кабель','BOOL',''),(43,6,0,'макросы','BOOL',''),(44,6,0,'память для макросов','SLIDER','{\"unit\": \"Мб\", \"if\": {\"parentId\": 43, \"value\": 1}}'),(45,6,0,'клавиши для макросов','SLIDER','{\"unit\": \"шт\"}');
/*!40000 ALTER TABLE `chars` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-28 22:45:26
