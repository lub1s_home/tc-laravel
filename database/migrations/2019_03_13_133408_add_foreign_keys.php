<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->index('name_singular');
            $table->index('name_plural');
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->dropIndex('devices_device_type_id_brand_id_name_release_date_index');
            $table->foreign('type_id')
                ->references('id')
                ->on('types');
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands');
            $table->index('name');
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->index('name');
        });

        Schema::table('questions', function (Blueprint $table) {
            // Комментировать эту строку, если буду делать php artisan migrate:rollback --step=1
            $table->dropIndex('questions_typeid_index');

            $table->foreign('type_id')
                ->references('id')
                ->on('types');
        });

        Schema::table('reviews', function (Blueprint $table) {
            // Комментировать эту строку, если буду делать php artisan migrate:rollback --step=1
            $table->dropIndex('reviews_deviceid_index');

            $table->index('is_published');
            $table->foreign('device_id')
                ->references('id')
                ->on('devices');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->foreign('question_id')
                ->references('id')
                ->on('questions');
            $table->foreign('review_id')
                ->references('id')
                ->on('reviews')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->dropIndex('types_name_singular_index');
            $table->dropIndex('types_name_plural_index');
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->index(['type_id', 'brand_id', 'name', 'release_date']);
            $table->dropForeign('devices_type_id_foreign');
            $table->dropForeign('devices_brand_id_foreign');
            $table->dropIndex('devices_name_index');
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->dropIndex('brands_name_index');
        });

        Schema::table('questions', function (Blueprint $table) {
//            $table->index('typeId'); // не сработает, т.к. имя колонки поменялось.
            $table->dropForeign('questions_type_id_foreign');
        });

        Schema::table('reviews', function (Blueprint $table) {
//            $table->index('deviceId'); // не сработает, т.к. имя колонки поменялось.
            $table->dropIndex('reviews_is_published_index');
            $table->dropForeign('reviews_device_id_foreign');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->dropForeign('ratings_question_id_foreign');
            $table->dropForeign('ratings_review_id_foreign');
        });
    }
}
