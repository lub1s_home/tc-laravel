<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceCharsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('device_chars', static function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('device_id')->nullable();
            $table->foreign('device_id')
                ->references('id')
                ->on('devices');

            $table->unsignedInteger('char_id')->nullable();
            $table->foreign('char_id')
                ->references('id')
                ->on('chars');

            $table->float('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('device_chars');
    }
}
