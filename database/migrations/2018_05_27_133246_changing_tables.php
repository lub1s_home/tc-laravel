<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_types', function (Blueprint $table) {
            $table->renameColumn('name_plural', 'namePlural');
            $table->renameColumn('name_singular', 'nameSingular');
            $table->renameColumn('name_prepositional', 'namePrepositional');
        });

        Schema::table('device_brands', function (Blueprint $table) {
            $table->renameColumn('site_url', 'siteUrl');
        });

        Schema::table('devices', function (Blueprint $table) {
            $table->renameColumn('device_type_id', 'typeId');
            $table->renameColumn('brand_id', 'brandId');
            $table->renameColumn('last_modified', 'lastModified');
            $table->renameColumn('release_date', 'releaseDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
