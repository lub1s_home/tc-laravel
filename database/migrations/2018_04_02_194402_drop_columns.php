<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_brands', function (Blueprint $table) {
            $table->dropColumn('wordstat');
        });
        Schema::table('device_types', function (Blueprint $table) {
            $table->dropColumn('wordstat');
        });
        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn(['al_aliexpress', 'al_ozon']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
