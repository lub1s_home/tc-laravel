<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactoringAllColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->renameColumn('siteUrl', 'site_url');
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->renameColumn('typeId', 'type_id');
        });

        Schema::table('ratings', function (Blueprint $table) {
            $table->renameColumn('reviewId', 'review_id');
            $table->renameColumn('questionId', 'question_id');
        });

        Schema::table('types', function (Blueprint $table) {
            $table->renameColumn('namePlural', 'name_plural');
            $table->renameColumn('nameSingular', 'name_singular');
            $table->renameColumn('namePrepositional', 'name_prepositional');
        });

        Schema::table('videos', function (Blueprint $table) {

            //Сначала дропаем внешний ключ, потом индекс!

            $table->dropForeign('videos_deviceid_foreign');
            $table->dropIndex('videos_deviceid_foreign');
            $table->renameColumn('deviceId', 'device_id');
            $table->foreign('device_id')
                ->references('id')
                ->on('devices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
