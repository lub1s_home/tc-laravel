<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('device_type_id')->default(0);
            $table->unsignedInteger('brand_id')->default(0);
            $table->string('name', 30);
            $table->unsignedInteger('last_modified')->default(0);
            $table->unsignedInteger('release_date')->default(0);
            $table->string('al_aliexpress');
            $table->unsignedInteger('al_ozon')->default(0);

            $table->index(['device_type_id', 'brand_id', 'name', 'release_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
