<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('char_groups', static function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('device_type_id');
            $table->foreign('device_type_id')
                ->references('id')
                ->on('types');

            $table->string('name');
            $table->unsignedTinyInteger('sort')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('char_groups');
    }
}
