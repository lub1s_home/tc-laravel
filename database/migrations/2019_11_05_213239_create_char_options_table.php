<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('char_options', static function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('char_id')->nullable();
            $table->foreign('char_id')
                ->references('id')
                ->on('chars');

            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('char_options');
    }
}
